@extends('admin.adminLayout')

@section('title')
@if($izmena) {{$clanak->naslov}} @else Novi clanak @endif
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<a class="breadcrumb-item" href="/admin/blog">Blog</a>
<span class="breadcrumb-item active">@if($izmena) {{$clanak->naslov}} @else Novi članak @endif</span>
@stop

@section('heder-h1')
@if($izmena) {{$clanak->naslov}} @else Novi članak @endif
@stop

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('assets/js/plugins/dropzonejs/dropzone.css')}}">
    <script src="{{asset('js/adminClanak.js')}}"></script>
@endsection

@section('scriptsBottom')
    <script src="{{asset('assets/js/plugins/dropzonejs/dropzone.js')}}"></script>
    <script src="{{asset('js/dropzoneSlikeClanci.js')}}"></script>
@endsection

@section('main')

<div class="row gutters-tiny">
    <!-- Out of Stock -->
    <div class="col-md-3 col-xl-3">

        <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-clanak-submit-button').click()">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="si si-settings fa-2x text-success"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Stock -->

@if($izmena)
    @if(!$clanak->sakriven)
        <!-- Delete Product -->
            <div class="col-md-3 col-xl-3">
                <form id="forma-obrisi-clanak" method="POST" action="/admin/obrisiClanak/{{$clanak->id}}">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-clanak').submit();">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-trash fa-2x text-danger"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-danger">
                                    <i class="fa fa-times"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši članak</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>

        @else
            <div class="col-md-3 col-xl-3">
                <form id="forma-restauriraj-clanak" method="POST" action="/admin/restaurirajClanak/{{$clanak->id}}">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-clanak').submit();">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-warning">
                                    <i class="fa fa-undo"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj članak</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>
        @endif
    @endif
</div>

<form id="forma-sacuvaj" method="post" @if($izmena) action="/admin/sacuvajClanak/{{$clanak->id}}" @else action="/admin/sacuvajClanak/-1" @endif>
    {{csrf_field()}}

    <h2 class="content-heading">Informacije o članku</h2>
    <div class="row gutters-tiny">
        <div class="col-md-8">

            <div class="block block-rounded block-themed">

                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">Članak</h3>
                </div>
                <div class="block-content block-content-full">
                    <div class="form-group row">
                        <label class="col-12">Naslov</label>
                        <div class="col-12">
                            <input type="text" maxlength="254" class="form-control" name="naslov"  @if($izmena) value="{{$clanak->naslov}}" @endif required/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12">Uvod</label>
                        <div class="col-12">
                            <input type="text" maxlength="254" class="form-control" name="uvod"  @if($izmena) value="{{$clanak->uvod}}" @endif required/>
                        </div>
                    </div>

                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Sadržaj</h3>

                        </div>
                        <div class="block-content">

                            <div class="form-group row">
                                <div class="col-12">
                                    <!-- CKEditor Container -->
                                    <textarea id="js-ckeditor" name="tekst" placeholder="Unesi tekst članka...">
                                            @if($izmena)
                                            {{$clanak->tekst}}
                                        @else

                                        @endif
                                        </textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="block block-rounded block-themed">

                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">Slika</h3>

                </div>

            @if($izmena and File::exists(public_path('/images/clanci/temp/glavna.jpg')))
                <!-- Existing Images -->
                    <div id="slike-glavna.jpg" class="row gutters-tiny items-push">
                        <div class="col-sm-12 col-xl-12">
                            <div class="options-container">
                                <img class="img-fluid options-item" src="{{asset('images/clanci/temp/glavna.jpg')}}" alt="">
                                <div class="options-overlay bg-black-op-75">
                                    <div class="options-overlay-content">
                                        <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript: obrisiSliku('glavna.jpg');">
                                            <i class="fa fa-times"></i> Obriši
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Existing Images -->
                @endif
                <div class="dropzone" id="glavna-dropzone-div" name="mainFileUploader">
                    <input type="hidden" id="glavna-dropzone" name="glavna" value="0"/>
                    <div class="fallback">
                        <input name="file" type="file" />
                    </div>
                </div>




            </div>
        </div>
    </div>
    <input type="submit" id="forma-clanak-submit-button" style="display:none"/>
</form>
<!-- END CKEditor -->
@stop