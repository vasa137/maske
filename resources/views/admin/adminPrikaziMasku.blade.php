@extends('admin.adminLayout')

@section('title')
    Prikaz samostalno dizajnirane maske - stavka #{{$stavkaPorudzbina->id}}, porudzbina #{{$stavkaPorudzbina->id_porudzbina}}
@stop

@section('heder-h1')
   {{$stavkaPorudzbina->brend->naziv}} - @if($stavkaPorudzbina->id_proizvod == \App\Proizvod::$ID_PRAVLJENA) Samostalni dizajn @else {{$stavkaPorudzbina->proizvod->naziv}} @endif
@stop
@section('scriptsTop')

    <!-- End Contribute CSS Files -->

    <!-- Custom CSS Files -->
    <link rel="stylesheet" href="{{asset('css/stylesheets/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('css/stylesheets/font.css')}}" />
    <link rel="stylesheet" href="{{asset('css/stylesheets/styles.css')}}" />
@stop




@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <span class="breadcrumb-item active">{{$stavkaPorudzbina->brend->naziv}} -  @if($stavkaPorudzbina->id_proizvod == \App\Proizvod::$ID_PRAVLJENA) Samostalni dizajn @else {{$stavkaPorudzbina->proizvod->naziv}} @endif  - stavka #{{$stavkaPorudzbina->id}}, porudzbina #{{$stavkaPorudzbina->id_porudzbina}}</span>
@stop

@section('main')
    <div class="col-xs-12" style="display: flex; justify-content: center; align-items: center;">
        <div id="cp-sel-Device">
            <div id="cp-device-ori" style="position: relative;">
                <div id="cp-mask-img">
                    <img alt="" src="{{asset('images/brendovi/' . $stavkaPorudzbina->brend->id . '/' . $stavkaPorudzbina->brend->naziv .'.png') }}">
                </div>
                <div id="cp-gridme">
                    <style>
                        .sqr{
                            height: 100%;
                        }
                    </style>
                    <div class="container-fluid w-sqr" style="padding-left:0 !important; padding-right:0 !important;">


                        <div class="col-md-12 sqr" style="visibility: hidden;">
                            @if($stavkaPorudzbina->id_proizvod == \App\Proizvod::$ID_PRAVLJENA)
                                <span class="wrap-img-drag no-visible" style="background-image:url({{\Storage::url('stavke/' . $stavkaPorudzbina->id . '/maska.png')}});">

                                </span>
                            @else
                                <span class="wrap-img-drag no-visible" style="background-image:url('/images/proizvodi/{{$stavkaPorudzbina->id_proizvod}}/glavna/{{$stavkaPorudzbina->proizvod->nazivGlavneSlike()}}.jpg');">

                                </span>
                            @endif
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

@stop