@extends('admin.adminLayout')

@section('title')
    @if($izmena)
        Opcija - {{$opcija->naziv}}
    @else
        Nova opcija
    @endif
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <a class="breadcrumb-item" href="/admin/opcije">Opcije</a>
    <span class="breadcrumb-item active">@if($izmena){{$opcija->naziv}} @else Nova opcija @endif</span>
@stop

@section('heder-h1')
@if($izmena){{$opcija->naziv}} @else Nova opcija @endif
@stop


@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.css')}}">
@endsection
<!-- Bootstrap Colorpicker (.js-colorpicker class is initialized in Codebase() -> uiHelperColorpicker()) -->
<!-- For more info and examples you can check out https://github.com/itsjavi/bootstrap-colorpicker/ -->
@section('scriptsBottom')
    <script src="{{asset('assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/be_forms_plugins.js')}}"></script>

    <script>
        jQuery(function () {
            // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
            Codebase.helpers(['colorpicker']);
        });
    </script>
@endsection

@section('main')
    <div class="row gutters-tiny">
    @if($izmena)

            <!-- In Orders -->
            <div class="col-md-3 col-xl-3">
                <a class="block block-rounded block-link-shadow" >
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-shopping-basket fa-2x text-info"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$opcija->broj_proizvoda}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Različitih proizvoda</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END In Orders -->
    @endif
    <!-- Stock -->
        <div class="col-md-3 col-xl-3">

            <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-opcija-submit-button').click()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="si si-settings fa-2x text-success"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Stock -->

    @if($izmena)
        @if(!$opcija->sakriven)
            <!-- Delete Product -->

                <div class="col-md-3 col-xl-3">
                        <form id="forma-obrisi-opciju" method="POST" action="/admin/obrisiOpciju/{{$opcija->id}}">
                            {{csrf_field()}}
                            <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-opciju').submit();">
                                <div class="block-content block-content-full block-sticky-options">
                                    <div class="block-options">
                                        <div class="block-options-item">
                                            <i class="fa fa-trash fa-2x text-danger"></i>
                                        </div>
                                    </div>
                                    <div class="py-20 text-center">
                                        <div class="font-size-h2 font-w700 mb-0 text-danger">
                                            <i class="fa fa-times"></i>
                                        </div>
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši opciju</div>
                                    </div>
                                </div>
                            </a>
                        </form>
                </div>



            @else
                <div class="col-md-3 col-xl-3">
                    <form id="forma-restauriraj-opciju" method="POST" action="/admin/restaurirajOpciju/{{$opcija->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-opciju').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-warning">
                                        <i class="fa fa-undo"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj opciju</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>
        @endif
    @endif
    <!-- END Delete Product -->
    </div>
    <!-- END Overview -->
    <form id="forma-opcija" method="POST" @if($izmena) action="/admin/sacuvajOpciju/{{$opcija->id}}" @else action="/admin/sacuvajOpciju/-1" @endif>
    {{csrf_field()}}
    <!-- Update Product -->
        <h2 class="content-heading">Informacije o opciji</h2>
        <div class="row gutters-tiny">
            <!-- Basic Info -->
            <div class="col-md-7">
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Informacije</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="form-group row">
                            <label class="col-12" >Naziv</label>
                            <div class="col-12 input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-info"></i>
                                </span>
                                </div>
                                <input id="naziv"  maxlength="254" type="text" class="form-control" name="naziv" @if($izmena) value="{{$opcija->naziv}}" @endif required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12" for="example-colorpicker2">Opis</label>
                            <div class="col-lg-12">
                                <div class="js-colorpicker input-group">
                                    <input type="text" class="form-control" id="example-colorpicker2" name="opis" @if($izmena) value="{{$opcija->opis}}" @else value="#000000" @endif>
                                    <div class="input-group-append input-group-addon">
                                        <div class="input-group-text">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- END Basic Info -->

            <!-- More Options -->
            <div class="col-md-5">
                <!-- Status -->
                <div class="block block-rounded block-themed">


                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-gd-primary">
                            <h3 class="block-title">Grupa opcija</h3>
                        </div>
                        <div class="block-content block-content-full row">
                            <div class="col-sm-12">
                                @foreach($grupeOpcija as $grupaOpcija)
                                    <label class="css-control css-control-primary css-radio">
                                        <input type="radio" class="css-control-input" name="grupaOpcija" value="{{$grupaOpcija->id}}" @if(!$izmena or ($izmena and $opcija->id_grupa_opcija == $grupaOpcija->id)) checked @endif >
                                        <span class="css-control-indicator"></span> {{$grupaOpcija->naziv}}
                                    </label>
                                    <br/>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- END More Options -->

        <!-- END Update Product -->
        <input type="submit" id="forma-opcija-submit-button" style="display:none"/>
    </form>
@stop