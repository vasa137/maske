@extends('layout')

@section('title')
NASLOVNA
@stop

@section('act_naslovna')
class="active"
@stop


@section('main')
<div class="telefon">

<div class="slider-wrapper">
    <div class="swiper-button-prev visible-lg"></div>
    <div class="swiper-button-next visible-lg"></div>
    <div class="swiper-container" data-parallax="1" data-auto-height="1">
       <div class="swiper-wrapper">
<!--
           <div class="swiper-slide">
                
                <video style="position: fixed;
                  right: 0; 
                  bottom: 0;
                  min-width: 100%; 
                  min-height: 100%;
                  width: auto; 
                  height: auto;
                  z-index: -100;" 
                  playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                     <source src="img/video-slider.mp4" type="video/mp4">
                </video>
            
                <div class="container cell-view page-height" style="text-align: center;  position: absolute;">
                    <div style="margin: 0 auto; width: 50%">
                        <h1 style=" margin-top: 35%;" class="h1 light">UNESITE MODEL</h1>
                        <form action="/izaberi_brend_proizvoda" method="GET">
                            {{csrf_field()}}
                            <select name="id_brend" style="margin: 0 auto;" class="SlectBox" onchange="this.form.submit()">
                                <option disabled="disabled" selected="selected">IZABERITE UREĐAJ</option>
                                @foreach($brendovi as $brend)
                                    <option value="{{$brend->id}}" @if($izabraniBrend != null && $brend->id == $izabraniBrend) selected @endif>{{$brend->naziv}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                    
                    <div class="empty-space col-xs-b80 col-sm-b0"></div>
                </div>
           </div>
       -->
           <div class="swiper-slide" style="background-image: url(img/maske-za-telefone-shop.jpg);">
                <div class="container cell-view page-height" style="text-align: center; ">
                        <div style="margin: 0 auto; width: 50%">
                            <h1 style=" margin-top: 30%;" class="h1 light"><br></h1>
                            <form action="/izaberi_brend_proizvoda" method="GET">
                                {{csrf_field()}}
                                <select name="id_brend" style="margin: 0 auto;" class="SlectBox" onchange="this.form.submit()">
                                    <option disabled="disabled" selected="selected">UNESITE MODEL VAŠEG TELEFONA</option>
                                    @foreach($brendovi as $brend)
                                        <option value="{{$brend->id}}">{{$brend->naziv}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    <div class="empty-space col-xs-b80 col-sm-b0"></div>
                </div>
           </div>
           
       </div>
       <div class="swiper-pagination swiper-pagination-white"></div>
    </div>
</div>
<!--
<div class="container">
    <div class="grid">
        <div class="grid-sizer"></div>
        <div class="grid-item">
            <div class="gallery-grid-item style-1">
                <img src="img/baneri/6.jpg" alt="" />
                
            </div>
        </div>
        <div class="grid-item w50">
            <div class="gallery-grid-item style-1">
                <img src="img/baneri/1.jpg" alt="" />
                
            </div>
        </div>
        <div class="grid-item w50">
            <div class="gallery-grid-item style-1">
                <img src="img/baneri/3.jpg" alt="" />
                
            </div>
        </div>
        <div class="grid-item">
            <div class="gallery-grid-item style-1">
                <img src="img/baneri/8.jpg" alt="" />
                
            </div>
        </div>
        <div class="grid-item w50">
            <div class="gallery-grid-item style-1">
                <img src="img/baneri/9.jpg" alt="" />
                
            </div>
        </div>

        <div class="grid-item w50">
            <div class="gallery-grid-item style-1">
                <img src="img/baneri/5.jpg" alt="" />
               
            </div>
        </div>
    </div>
</div>

<div class="empty-space col-xs-b35 col-md-b70"></div>
<div class="empty-space col-xs-b35 col-md-b70"></div>

<div class="container">
    <div class="text-center">
        <div class="simple-article size-3 grey uppercase col-xs-b5">PORUČITE BILO KOJI DIZAJN SA NAŠEG INSTAGRAM PROFILA ZA SVE MODELE MOBILNIH TELEFONA</div>
        <div class="h2">INSTA SHOP</div>
        <div class="title-underline center"><span></span></div>
    </div>
</div>
<div class="row nopadding">
    <div class="col-md-3">
        <img width="100%" src="img/insta/1.jpg">
    </div>
    <div class="col-md-3">
        <img width="100%" src="img/insta/2.jpg">
    </div>
    <div class="col-md-3">
        <img width="100%" src="img/insta/3.jpg">
    </div>
    <div class="col-md-3">
        <img width="100%" src="img/insta/4.jpg">
    </div>
    <div class="col-md-3">
        <img width="100%" src="img/insta/5.jpg">
    </div>
    <div class="col-md-3">
        <img width="100%" src="img/insta/6.jpg">
    </div>
    <div class="col-md-3">
        <img width="100%" src="img/insta/7.jpg">
    </div>
    <div class="col-md-3">
        <img width="100%" src="img/insta/8.jpg">
    </div>
</div>

    
<div class="empty-space col-xs-b35 col-md-b70"></div>

<a href="/prodavnica">
<img style="width: 80%; padding-bottom: 20px; padding-top: 10px; display: block; margin-left: auto;margin-right: auto;" 
src="img/maske-za-telefone-shop.jpg" >
</a>  -->  
<a href="/dizajniraj">
<img style="width: 80%; padding-bottom: 20px; padding-top: 10px; display: block; margin-left: auto;margin-right: auto;"
 src="img/maske-za-telefone-dizajniraj.jpg" >
</a>
<a href="/insta">
<img style="width: 80%; padding-bottom: 20px; padding-top: 10px; display: block; margin-left: auto;margin-right: auto;" 
 src="img/maske-za-telefone-insta-shop.jpg">
</a>

</div>



<div class="komp slider-wrapper">

<a href="/prodavnica">
<img style="width: 100%; padding-bottom: 20px; padding-top: 50px;" src="img/maske-za-telefone-shop.jpg">
</a>
<a href="/dizajniraj">
<img style="width: 100%; padding-bottom: 20px;" src="img/maske-za-telefone-dizajniraj.jpg">
</a>
<a href="/insta">
<img style="width: 100%;" src="img/maske-za-telefone-insta-shop.jpg">
</a>
</div>


@stop