@extends('layout')

@section('title')
    DIZAJNIRAJ
@stop

@section('act_dizajn')
class="active"
@stop


@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/stylesheets/font-awesome.css')}}" />
    <!-- End Contribute CSS Files -->

    <!-- Custom CSS Files -->
    <link rel="stylesheet" href="{{asset('css/stylesheets/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('css/stylesheets/font.css')}}" />
    <link rel="stylesheet" href="{{asset('css/stylesheets/styles.css')}}" />
    <link rel="stylesheet" href="{{asset('css/stylesheets/cropper.css')}}" />
@stop

@section('scriptsBottom')
    <!-- Contribute JS Files -->
    <script type="text/javascript" src="{{asset('js/cp-lib-min.js')}}"></script>
    <!-- End Contribute JS Files -->
    <script type="text/javascript" src="{{asset('js/dizajniraj.js')}}"></script>
    <script>
        ucitajBrendove('{!! addslashes(json_encode($brendovi)); !!}');
    </script>
    <!-- Custom JS Files -->
    <script type="text/javascript" src="{{asset('js/mycase.js')}}"></script>
    <!-- Custom JS Files -->

    <!-- Example Usage Pluging Mycase -->
    <script>
        $('#wrap-phone').Mycase();
    </script>
@stop

@section('main')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. Želite li da pregledate korpu?', 'linkUspesno' => '/korpa' ,'yesButtonText' => 'Da' ,'noButtonText' => 'Ne'])

<br/><br/>
<div id="mycase" class="container">
    <div id="wrap-phone" class="row">
        <div class="col-sm-4 col-sm-push-8  col">
            <div id="accordion-step">
                <div class="step step1 actived-bl">
                    <a href="javascript:void(0);" class="btn-open">-1- Izaberite model telefona</a>
                    <div class="list">
                        <ul class="cp-device">
                            <!-- Call Data From Json File data_apple.json -->
                        </ul>
                    </div>
                </div>
                <!--
                <div class="step step2 disabled">
                    <a href="javascript:void(0);" class="btn-open">-2- Select Type of case</a>
                    <div class="list">
                        <ul class="cp-model">
                            <li class="casetypeclear">
                                <a href="#" class="clear">
                                    <img src="img/clear.png" alt="">
                                    <span>Clear</span>
                                </a>
                            </li>
                            <li class="casetypewhite">
                                <a href="#" class="white">
                                    <img src="img/white.png" alt="">
                                    <span>White</span>
                                </a>
                            </li>
                            <li class="casetypeblack">
                                <a href="#" class="black">
                                    <img src="img/black.png" alt="">
                                    <span>Black</span>
                                </a>
                            </li>
                            <li class="casetypetpu">
                                <a href="#" class="tpu">
                                    <img src="img/tpu.png" alt="">
                                    <span>TPU</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                -->
                <!--
                <div class="step step3 disabled">
                    <a href="javascript:void(0);" class="btn-open">-3- Select the layout</a>
                    <div class="list">

                        <div id="cp-sel-layout">
                            <ul>

                            </ul>
                        </div>

                    </div>
                </div>
                -->
                <div class="step step2 disabled">
                    <a href="javascript:void(0);" class="btn-open">-2- Odaberite fotografiju</a>
                    <div class="list">
                        <div id="cp-sel-Photos">
                            <br/>
                            <a href="javascript:void(0);" title="Upload" class="cp-btn-more-pic fileinput-button">
                                <span id="cp-img-lo-w">
                                    <i class="fa fa-refresh fa-spin" id="cp-img-lo"></i>
                                    <span class="cp-loader-number"></span>
                                </span>
                                <i class="fa fa-picture-o"></i>

                                <samp id="fromComputer">Iz galerije</samp>
                                <input id="fileupload" type="file" name="files[]" multiple>
                                <input type="hidden" class="cp-token" value="">
                            </a>
                            <div class="jscroll">
                                <ul>
                                    @foreach($ponudjeneSlike as $slika)
                                        <li><a href="#"><img src="{{asset('img/bg/' . $slika)}}" alt=""><i class="fa fa-plus"></i></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="step step3 disabled">
                    <a href="javascript:void(0);" class="btn-open">-3- Dodajte tekst</a>

                    <div class="list">

                        <!-- Bloc Select Text -->
                        <div id="cp-sel-Text">
                            <br/>
                            <div id="cp-textarea">
                                <textarea  class="cp-input-txt" placeholder="Your text here"> </textarea>
                                <ul id="cp-size-txt">
                                    <li><a id="s-t" class="size-cp s-t s-t-p active" href="javascript:void(0)">Mala</a></li>
                                    <li><a id="m-t" class="size-cp m-t m-t-p" href="javascript:void(0)">Srednja</a></li>
                                    <li><a id="g-t" class="size-cp g-t g-t-p" href="javascript:void(0)">Velika</a></li>
                                </ul>
                            </div>
                            <div class="cp-list-font">
                                <a href="javascript:void(0)" id="cp-gloria" class="font-cp cp-gloria active">Gloria</a>
                                <a href="javascript:void(0)" class="font-cp cp-montserrat" id="cp-montserrat">Montser.</a>
                                <a href="javascript:void(0)" class="font-cp cp-pacifico" id="cp-pacifico">Pacifico</a>
                                <a href="javascript:void(0)" class="font-cp cp-lobster" id="cp-lobster">Lobster</a>
                                <a href="javascript:void(0)" class="font-cp cp-yanone" id="cp-yanone">Kaffeesatz</a>
                                <a href="javascript:void(0)" class="font-cp cp-indie" id="cp-indie">Indie</a>
                                <a href="javascript:void(0)" class="font-cp cp-shadows" id="cp-shadows">Shadows</a>
                                <a href="javascript:void(0)" class="font-cp cp-poiret" id="cp-poiret">Poiret</a>
                                <a href="javascript:void(0)" class="font-cp cp-dancing" id="cp-dancing">Dancing</a>
                            </div>
                            <div class="cp-list-color">
                                <a id="rouge" class="rouge" href="javascript:void(0)"></a>
                                <a id="noir" class="noir" href="javascript:void(0)"></a>
                                <a id="gris" class="gris" href="javascript:void(0)"></a>
                                <a id="blanc" class="blanc cp-active-color-t" href="javascript:void(0)"></a>
                                <a id="marron" class="marron" href="javascript:void(0)"></a>
                                <a id="bleu" class="bleu" href="javascript:void(0)"></a>
                                <a id="vert" class="vert" href="javascript:void(0)"></a>
                                <a id="jaune" class="jaune" href="javascript:void(0)"></a>
                                <a id="rose" class="rose" href="javascript:void(0)"></a>
                                <a id="orange" class="orange" href="javascript:void(0)"></a>
                                <a id="gris-f" class="gris-f" href="javascript:void(0)"></a>
                                <a id="turk" class="turk" href="javascript:void(0)"></a>
                                <a id="orange-c" class="orange-c" href="javascript:void(0)"></a>
                                <a id="wet" class="wet" href="javascript:void(0)"></a>
                            </div>
                        </div>
                        <!-- End Bloc Select Text -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-5 col-sm-pull-1  col">
            <!-- Bloc Selected Device -->
            <div id="cp-sel-Device">
                <div id="cp-device-ori">
                    <div id="cp-mask-img">
                        <img alt="" src="">
                    </div>
                    <div id="cp-gridme"></div>
                    <div id="cp-gridme-cover">
                        <style>
                            .sqr{
                                height: 100%;
                            }
                        </style>
                        <div class="container-fluid w-sqr">
                            <div class="col-md-12 sqr"></div>
                        </div>
                    </div>
                    <pre id="cp-input-gen"></pre>
                </div>
                <div id="cp-add-cart">
                    <input type="hidden" id="izabraniBrend"/>
                    <a style="visibility: hidden" href="javascript:void(0);" class="cp-btn-save"><i class="fa fa-shopping-cart"></i>Dodaj u korpu</a>
                </div>

            </div>
            <!-- End Bloc Selected Device -->
        </div>
        <div class="col-sm-3 col-sm-pull-9 col">
            <div class="cp-info-dev">
                <span class="cp-title">Iphone7</span>
                <span class="cp-price">@if($samostalniProizvod->na_popustu) {{number_format($samostalniProizvod->cena_popust, 0, ',', '.')}} @else {{number_format($samostalniProizvod->cena, 0, ',', '.')}} @endif rsd</span>
            </div>
            <!--
            <div class="cp-btn-action">
                <a href="javascript:void(0)" class="cp-btn-shuffle"><i class="fa fa-random"></i>Shuffle</a>
                <a href="javascript:void(0)" class="cp-btn-reset"><i class="fa fa-refresh"></i>Reset</a>
            </div>

            -->
        </div>
        <!-- Bloc Crop Image -->
        <div id="cp-crop-image"><div id="cp-cropit-me"></div></div>
        <!-- End Bloc Crop Image -->
    </div>
</div>
<!-- End Main Bloc -->

<div class="overlay-bg"><div class="loader-canvas"><span class="progress-title">Čuvanje dizajna</span><div class="progress"></div><p class="progress-txt"></p></div></div>
@stop