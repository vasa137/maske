@extends('layout')


@section('title')
KORPA
@stop


@section('main')
<div class="container">
    <div class="empty-space col-xs-b15 col-sm-b30"></div>
    <div class="breadcrumbs">
        <a href="/">Naslovna</a>
        <a href="#">Korpa</a>
    </div>
    <div class="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
    <div class="text-center">
        <div class="simple-article size-3 grey uppercase col-xs-b5">KORPA</div>
        <div class="h2">Pregled korpe</div>
        <div class="title-underline center"><span></span></div>
    </div>
</div>

<div class="empty-space col-xs-b35 col-md-b70"></div>

<div class="container" id="korpaPrikaz">
    @if(count($stavke) == 0)
        <h3>Korpa je prazna.</h3>
    @else
    <div id="tabele-div" >
        <table class="cart-table">
            <thead>
                <tr>
                    <th style="width: 95px;"></th>
                    <th>Proizvod</th>
                    <th>Kupon</th>
                    <th style="width: 150px;">Cena</th>
                    <th style="width: 260px;">Količina</th>
                    <th style="width: 150px;">Ukupno</th>
                    <th style="width: 70px;"></th>
                </tr>
            </thead>
            <tbody id="tabela-korpa">
            @foreach($stavke as $stavka)
                <tr id="stavka-proizvod-{{$stavka->rowId}}">
                    @include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da obrišete proizvod iz korpe?', 'linkUspesno' => 'javascript:obrisiIzKorpe(\''. $stavka->rowId . '\',\'' .  explode('_',$stavka->id)[0] .'\')', 'dialogId' => $stavka->rowId])
                    <td data-title=" ">
                        @if( intval(explode('_',$stavka->id)[0]) > 0)
                            @if(File::exists(public_path('/images/proizvodi/' . explode('_',$stavka->id)[0]  . '/glavna/' . $stavka->nazivGlavneSlike . '.jpg')))
                                <a class="cart-entry-thumbnail" href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->nazivGlavneSlike))?>/{{explode('_',$stavka->id)[0]}}">
                                    <div id="cp-sel-Device" style="padding-top:0px !important;">
                                        <div id="cp-device-ori" style="position: relative; width:70px; height:auto;">
                                            <div id="cp-mask-img">
                                                <img  alt="" src="{{asset('images/brendovi/' . explode('_',$stavka->id)[1] . '/' . \App\Brend::dohvatiSaId(explode('_',$stavka->id)[1])->naziv .'.png') }}">
                                            </div>
                                            <div id="cp-gridme">
                                                <style>
                                                    .sqr{
                                                        height: 100%;
                                                    }
                                                </style>
                                                <div class="container-fluid w-sqr" style="padding-left:0 !important; padding-right:0 !important;">


                                                    <div class="col-md-12 sqr" style="visibility: hidden;">
                                                        <span class="wrap-img-drag no-visible" style="background-image:url('{{asset('/images/proizvodi/'. explode('_',$stavka->id)[0] .'/glavna/' . $stavka->nazivGlavneSlike . '.jpg')}}');">
                                                        </span>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </a>
                            @endif
                        @else
                            <div id="cp-sel-Device" style="padding-top:0px !important;">
                                <div id="cp-device-ori" style="position: relative; width:70px; height:auto;">
                                    <div id="cp-mask-img">
                                        <img  alt="" src="{{asset('images/brendovi/' . explode('_',$stavka->id)[1] . '/' . \App\Brend::dohvatiSaId(explode('_',$stavka->id)[1])->naziv .'.png') }}">
                                    </div>
                                    <div id="cp-gridme">
                                        <style>
                                            .sqr{
                                                height: 100%;
                                            }
                                        </style>
                                        <div class="container-fluid w-sqr" style="padding-left:0 !important; padding-right:0 !important;">


                                            <div class="col-md-12 sqr" style="visibility: hidden;">
                                                        <span class="wrap-img-drag no-visible" style="background-image:url('{{asset('/images/maske/'. \Session::getId() . '/' .explode('_',$stavka->id)[0] .'/maska.png')}}');">
                                                        </span>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        @endif
                    </td>
                    <td data-title=" ">
                        <h6 class="h6">
                            @if(intval(explode('_',$stavka->id)[0]) > 0)
                                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->name))?>/{{explode('_',$stavka->id)[0]}}">
                                    {{$stavka->brend->naziv}} - {{$stavka->name}}
                                </a>
                            @else
                                {{$stavka->brend->naziv}} - Samostalni dizajn
                            @endif
                        </h6>
                    </td>
                    <td >
                        <div data-title="Kupon: " id="kupon-stavka-{{$stavka->rowId}}">@if($stavka->kupon != null) {{$stavka->kupon->kod}} ({{$stavka->kupon->popust*100}}%) @else / @endif</div>
                    </td>
                    <td data-title="Price: " id="stavka-price-{{$stavka->rowId}}">{{number_format($stavka->price, 0, ',', '.')}} rsd</td>
                    <td data-title="Quantity: ">
                        <div class="quantity-select">
                            <span onclick="azurirajKolicinuUKorpi('{!! $stavka->rowId !!}', '{!! explode('_',$stavka->id)[0]!!}', parseInt($('#qttotal-stavka-{{$stavka->rowId}}').html()) - 1 )" class="minus"></span>
                            <span id="qttotal-stavka-{{$stavka->rowId}}" class="number">{{$stavka->qty}}</span>
                            <span onclick="azurirajKolicinuUKorpi('{!! $stavka->rowId !!}', '{!! explode('_',$stavka->id)[0]!!}', parseInt($('#qttotal-stavka-{{$stavka->rowId}}').html()) + 1 )" class="plus"></span>
                        </div>
                    </td>
                    <td data-title="Total:" id="stavka-total-{{$stavka->rowId}}">{{$stavka->total(0, ',', '.')}} rsd</td>
                    <td data-title="">
                        <div class="button-close"  onclick="otvoriDialogSaId('{!! $stavka->rowId!!}')"></div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <br/><br/><br/>

        @if(count($stavkeVauceri) > 0)
            @include('include.korpaVauceri', ['stavkeVauceri', $stavkeVauceri])
        @endif
    </div>
    <div class="empty-space col-xs-b35"></div>
    <div class="row">
        <div class="col-sm-6 col-md-5 col-xs-b10 col-sm-b0">
            <div class="single-line-form">
                <input id="kod" class="simple-input" type="text" value="" placeholder="Unesite kod kupona" />
                <div class="button size-2 style-3">
                   

                        <span class="text">Primeni kupon</span>
                    
                    <input onclick="primeniKupon()" type="button" value="">

                </div>
            </div>
            <p id="kupon-text"></p>
        </div>
        <div class="col-sm-6 col-md-7 col-sm-text-right">
            <div class="buttons-wrapper">
                <a class="button size-2 style-2" href="/naplati">
                    <span class="button-wrapper">
                        <span class="text">Naplati</span>
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>
    <div class="row">
        <div class="col-md-6 col-xs-b50 col-md-b0">
            
        </div>
        <div class="col-md-6">
            <h4 class="h4">UKUPNO</h4>
            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        CENA
                    </div>
                    <div class="col-xs-6 col-xs-text-right">
                        <div class="h5" id="total-cena">{{number_format($total,0 , ',', '.')}} rsd</div>
                    </div>
                </div>
            </div>
            <!--
            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        POPUST (VAUCER 1000)
                    </div>
                    <div class="col-xs-6 col-xs-text-right">
                        <div class="h5">- 1,000 DIN</div>
                    </div>
                </div>
            </div>
            -->
            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        DOSTAVA
                    </div>

                    <div class="col-xs-6 col-xs-text-right">
                        <div class="h5" id="dostava">
                            @if(Cart::instance('korpa')->count() < 4)
                                249 rsd
                            @else
                                0 rsd
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        UKUPNO
                    </div>
                    <div class="col-xs-6 col-xs-text-right">
                        @if($total < 1990)
                            <div class="h4" id="total-cena-dostava">{{number_format($total + 280,0 , ',', '.')}} rsd</div>
                        @else
                            <div class="h4" id="total-cena-dostava">{{number_format($total,0 , ',', '.')}} rsd</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>
    @endif
</div>

        

@stop