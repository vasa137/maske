@extends('layout')


@section('title')
NAPLATI
@stop

@section('main')
<div class="container">
    <div class="empty-space col-xs-b15 col-sm-b30"></div>
    <div class="breadcrumbs">
        <a href="/">Naslovna</a>
        <a href="/korpa">Korpa</a>
        <a href="#">Naplati</a>
    </div>
    <div class="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
    <div class="text-center">
        <div class="simple-article size-3 grey uppercase col-xs-b5">NAPLATI</div>
        <div class="h2">Podaci za dostavu</div>
        <div class="title-underline center"><span></span></div>
    </div>
</div>

<div class="empty-space col-xs-b35 col-md-b70"></div>

<div class="container">
    <form action="/sacuvaj_porudzbinu" class="checkout-form" method="POST">
        {{csrf_field()}}
    <div class="row">

        <div class="col-md-6 col-xs-b50 col-md-b0">
            <h4 class="h4 col-xs-b25">Podaci za dostavu</h4>
            
            <div class="empty-space col-xs-b20"></div>
            <div class="row m10">
                <div class="col-sm-6">
                    <input maxlength="254" name="ime_prezime" class="simple-input" type="text" value="" placeholder="Ime i prezime*" required/>
                    <div class="empty-space col-xs-b20"></div>
                </div>
                <div class="col-sm-6">
                    <input maxlength="19"  name="telefon" class="simple-input" type="text" value="" placeholder="Telefon*" required/>
                    <div class="empty-space col-xs-b20"></div>
                </div>
            </div>
            <input maxlength="254" name="adresa" class="simple-input" type="text" value="" placeholder="Adresa*" required/>
            <div class="empty-space col-xs-b20"></div>
            
            <div class="row m10">
                <div class="col-sm-6">
                    <input maxlength="254" name="grad" class="simple-input" type="text" value="" placeholder="Grad*" required/>
                    <div class="empty-space col-xs-b20"></div>
                </div>
                <div class="col-sm-6">
                    <input type="number" min="10000" max="99999" name="zip" class="simple-input" value="" placeholder="Poštanski broj*" required/>
                    <div class="empty-space col-xs-b20"></div>
                </div>
            </div>

            
            <input maxlength="254" name="email" class="simple-input" type="email" value="mail@mail.com" placeholder="E-mail adresa*" hidden="" />
            <div class="empty-space col-xs-b20"></div>
            


            @if(Session::has('brend'))
                @if(Session::get('brend')==75)
                   
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="simple-article size-3 col-xs-b5">
                                U NAPOMENI <STRONG>ISPOD </STRONG> UNESITE MODEL VAŠEG UREĐAJA.
                                <br>Fotografija je informativnog karaktera. Dizajn maske će biti prilagođen modelu uređaja koji ste naveli u napomeni.
                            
                            </div>
                        </div>
                    </div>
                 @endif
            
            @endif
            <textarea name="napomena" class="simple-input" placeholder="Napomena"></textarea>
        </div>
        <div class="col-md-6">
            <h4 class="h4 col-xs-b25">Vaša porudžbina</h4>
   
            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        CENA 
                    </div>
                    <div class="col-xs-6 col-xs-text-right">
                        <div class=""><STRONG>{{number_format($total, 0, ',', '.')}} RSD</STRONG></div>
                    </div>
                </div>
            </div>
            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        DOSTAVA
                    </div>
                    <div class="col-xs-6 col-xs-text-right">
                        <div class="">@if(Cart::instance('korpa')->count() > 3) <STRONG>BESPLATNA</STRONG> @else 249 rsd @endif</div>
                    </div>
                </div>
            </div>
            <div class="empty-space col-xs-b50"></div>
            <div class="empty-space col-xs-b10"></div>
            <div class="button block size-2 style-3">
            

                    <span class="text">Poruči</span>
                
                <input type="submit"/>
            </div>
        </div>
    </div>
    </form>
</div>

<div class="empty-space col-xs-b35 col-md-b70"></div>
        

@stop