@extends('layout')


@section('title')
PROIZVOD {{$proizvod->naziv}}
@stop

@section('scriptsBottom')
    <script src="{{asset('js/klijentProizvod.js')}}"></script>
@stop

@section('main')
 <div class="container">
            <div class="empty-space col-xs-b15 col-sm-b30"></div>
            <div class="breadcrumbs">
                <a href="/">Naslovna</a>
                <a href="#">{{$proizvod->naziv}}</a>
            </div>
            <div class="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-5 col-xs-b30 col-sm-b0">
                            
                            <div class="main-product-slider-wrapper swipers-couple-wrapper">
                                <div class="swiper-container swiper-control-top">
                                   <div class="swiper-button-prev hidden"></div>
                                   <div class="swiper-button-next hidden"></div>
                                   <div class="swiper-wrapper">
                                       <div class="swiper-slide">
                                            <div class="swiper-lazy-preloader"></div>
                                           <div id="cp-sel-Device" style="padding-top:0 !important;">
                                               <div id="cp-device-ori" style="position: relative;">
                                                   <div id="cp-mask-img">
                                                       <img id="brend-image" alt="" src="{{asset('images/brendovi/' . $proizvod->brend->id . '/' . $proizvod->brend->naziv .'.png') }}">
                                                   </div>
                                                   <div id="cp-gridme">
                                                       <style>
                                                           .sqr{
                                                               height: 100%;
                                                           }
                                                       </style>
                                                       <div class="container-fluid w-sqr" style="padding-left:0 !important; padding-right:0 !important;">


                                                           <div class="col-md-12 sqr" style="visibility: hidden;">
                                                                <span class="wrap-img-drag no-visible" style="background-image:url('/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg');">
                                                                </span>

                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>

                                       </div>
                                       <!--
                                       <div class="swiper-slide">
                                            <div class="swiper-lazy-preloader"></div>
                                            <div class="product-big-preview-entry swiper-lazy" data-background="{{asset('img/shop/def.jpg')}}"></div>
                                       </div>
                                       <div class="swiper-slide">
                                            <div class="swiper-lazy-preloader"></div>
                                            <div class="product-big-preview-entry swiper-lazy" data-background="{{asset('img/shop/def.jpg')}}"></div>
                                       </div>
                                       -->
                                   </div>
                                </div>

                                <div class="empty-space col-xs-b30 col-sm-b60"></div>
<!--
                                <div class="swiper-container swiper-control-bottom" data-breakpoints="1" data-xs-slides="3" data-sm-slides="3" data-md-slides="4" data-lt-slides="4" data-slides-per-view="5" data-center="1" data-click="1">
                                   <div class="swiper-button-prev hidden"></div>
                                   <div class="swiper-button-next hidden"></div>
                                   <div class="swiper-wrapper">
                                       <div class="swiper-slide">
                                            <div class="product-small-preview-entry">
                                                <img height="100" src="{{asset('img/shop/def.jpg')}}" alt="" />
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="product-small-preview-entry">
                                                <img height="100" src="{{asset('img/shop/def.jpg')}}" alt="" />
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="product-small-preview-entry">
                                                <img height="100" src="{{asset('img/shop/def.jpg')}}" alt="" />
                                            </div>
                                        </div>
                                        

                                   </div>
                                </div>
                                -->
                            </div>

                        </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="simple-article size-3 grey col-xs-b5">
                                @if(!empty($proizvod->kategorije))
                                    @foreach($proizvod->kategorije as $kategorija)
                                        {{$kategorija->naziv}}&nbsp;
                                        @if($kategorija != $proizvod->kategorije[count($proizvod->kategorije) - 1])
                                            |&nbsp;
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            <div class="h3 col-xs-b25">{{$proizvod->naziv}}</div>
                            <div class="row col-xs-b25">
                                <div class="col-sm-6">
                                    <div class="simple-article size-5 grey">CENA:
                                        @if($proizvod->na_popustu)
                                            <span style="font-weight: bold;">{{number_format($proizvod->cena_popust, 0, ',', '.')}} RSD</span>&nbsp;&nbsp;&nbsp;<span class="line-through">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</span>
                                        @else
                                            <span style="font-weight: bold;">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="simple-article size-3 col-xs-b5">ŠIFRA : <span class="grey">{{$proizvod->sifra}}</span></div>
                                </div>
                            </div>
                            <div class="simple-article size-3 col-xs-b30">{{$proizvod->opis}}</div>
                            <div class="row col-xs-b40">
                                <div class="col-sm-4">
                                    <div class="h6 detail-data-title size-1">MODEL TELEFONA:</div>
                                </div>
                                <div class="col-sm-8">
                                    <select class="SlectBox" id="brendSelect" onchange="promeniBrend()">
                                        @foreach($brendovi as $brend)
                                            <option value="{{$brend->id}}" @if($brend->id == $proizvod->brend->id) selected @endif >{{$brend->naziv}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row col-xs-b40">
                                <div class="col-sm-4">
                                    <div class="h6 detail-data-title size-1">Količina:</div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="quantity-select">
                                        <span class="minus"></span>
                                        <span id="kolicina"  class="number">1</span>
                                        <span class="plus"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row m5 col-xs-b40">
                                <div class="col-sm-6 col-xs-b10 col-sm-b0">
                                    <a class="button size-2 style-2 block" href="javascript:dodajUKorpu('{!! $proizvod->id !!}', $('#kolicina').html(), $('#brendSelect').val())">
                                    
                                            
                                            <span class="text">Dodaj u korpu</span>
                                        
                                    </a>
                                </div>
                                <!--
                                <div class="col-sm-6">
                                    <a class="button size-2 style-1 block noshadow" href="#">
                                    <span class="button-wrapper">
                                        <span class="icon"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                                        <span class="text">Dodaj u listu želja</span>
                                    </span>
                                </a>
                                </div>
                                -->
                            </div>
                            @if(Session::has('brend'))
                                @if(Session::get('brend')==75)
                                   
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="simple-article size-3 col-xs-b5">
                                                U NAPOMENI<STRONG> PRILIKOM NAPLATE </STRONG> UNESITE MODEL VAŠEG UREĐAJA.
                                                <br>Fotografija je informativnog karaktera. Dizajn maske će biti prilagođen modelu uređaja koji ste naveli u napomeni.
                                            
                                            </div>
                                        </div>
                                    </div>
                                 @endif
                            
                            @endif

                            <!--
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="h6 detail-data-title size-2">share:</div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="follow light">
                                        <a class="entry" href="#"><i class="fa fa-facebook"></i></a>
                                        <a class="entry" href="#"><i class="fa fa-twitter"></i></a>
                                        <a class="entry" href="#"><i class="fa fa-linkedin"></i></a>
                                        <a class="entry" href="#"><i class="fa fa-google-plus"></i></a>
                                        <a class="entry" href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </div>
                                </div>
                            </div>
                            -->
                        </div>
                        
                    </div>
                </div>
            </div>

                     
        </div>

  

@stop