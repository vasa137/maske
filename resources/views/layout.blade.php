<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/stylesheets/font.css')}}" />
    <link rel="stylesheet" href="{{asset('css/stylesheets/styles.css')}}" />

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/bootstrap.extension.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/swiper.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/sumoselect.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117964354-15"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-117964354-15');
    </script>


     <style >
        
        .header-top-black {
              position: fixed; 
              top: 0; 
              width: 100%; 
              padding-top: 6px;
              
              background: black;
              color: white;
              text-align: center;
            }
    
        .telefon {
            display:none;
        }

        .komp{
            display:block;
        }
     
        @media only screen and (min-device-width:600px){
            
        .telefon {
                display:block;
                }
        
        .komp{
            display:none;
            }
        }
        
    </style>

    @yield('scriptsTop')

    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />
  	<title>@yield('title')</title>
</head>
<body>
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. <br/> Da li želite da pregledate korpu?', 'linkUspesno' => '/korpa'])
    <!-- LOADER -->
    <div id="loader-wrapper"></div>

    <div id="content-block">
        <!-- HEADER -->
        <header style="background: white">
            <div class="header-top" style="display:inline;">
                <div class="content-margins">
                    <div class="row telefon">
                        <div class="col-md-7 col-sm-12 col-xs-12">
                            <div class=" entry"><b><a href="mailto:office@mrcase.rs">office@mrcase.rs</a></b> </div>
                            <div class=" entry"><b><a href="tel:062 137 96 17">062 137 96 17</a></b></div>

                            

                            
                            
                        </div>
                        <div class=" col-md-5 col-md-text-right">


                        
                            <div class="hidden-xs hidden-sm entry "><a   href="http://facebook.com/MRCASE.RS" target="_blank" rel="nofollow"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
                            <div class="hidden-xs hidden-sm entry "><a  target="_blank" rel="nofollow" href="https://www.instagram.com/mrcase.rs/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>


                            


                            <div class="entry hidden-xs hidden-sm cart" @if(Route::getCurrentRoute()->getName() != 'pregled_korpe'  && Route::getCurrentRoute()->getName() != 'naplati') id="korpaInclude" @endif>
                                @include('include.korpaNavbar')
                            </div>
                            <div class="hamburger-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="komp">
                        <div class="row">
                            <div class="entry komp hidden-md hidden-lg"><a href="fb://MRCASE.RS" target="_blank" rel="nofollow"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
                             <div class="entry komp hidden-md hidden-lg"><a  target="_blank" rel="nofollow" href="instagram://mrcase.rs/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>
                             <div class=" entry"><b><a href="tel:062 137 96 17">062 137 96 17</a></b></div>
                             <div class="entry hidden-md cart" @if(Route::getCurrentRoute()->getName() != 'pregled_korpe'  && Route::getCurrentRoute()->getName() != 'naplati') id="korpaInclude" @endif>
                                @include('include.korpaNavbar')
                            </div>
                             <div class="hamburger-icon text-right">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                      
                        
                    </div>
                </div>
            </div>
            <div class="header-bottom" style="background-color:#FFFFFF; display:inline;">
                <div class="content-margins">
                    <div class="row">
                        <div class="col-xs-3 col-sm-5">
                            <a id="logo" href="/"><img src="{{asset('img/logo.png')}}" alt="" /></a>
                        </div>
                        <div class="col-xs-9 col-sm-7 text-right" >
                            <div class="nav-wrapper">
                                <div class="nav-close-layer"></div>
                                <nav >
                                    <ul>
                                        <li @yield('act_naslovna')>
                                            <a href="/">Naslovna</a>
                                        </li>
                                        <li @yield('act_prodavnica')>
                                            <a href="/prodavnica">PRODAVNICA</a>
                                        </li>
                                        <li @yield('act_insta')>
                                            <a href="/insta">INSTA-SHOP</a>
                                        </li>
                                        <li @yield('act_dizajn')>  
                                            <a href="/dizajniraj">DIZAJNIRAJ SVOJU MASKU</a>
                                        </li>
                                        
                                        <!--
                                        <li><a href="/kontakt">KONTAKT</a></li>
                                    -->
                                    </ul>
                                    <div class="navigation-title">
                                        Menu/Navigacija
                                        <div class="hamburger-icon active">
                                            
                                        </div>
                                    </div>
                                </nav>
                            </div>
<!--
                            <div class="header-bottom-icon "><i class="fa fa-facebook" aria-hidden="true"></i></div>
                            <div class="header-bottom-icon"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                        -->
                        </div>
                    </div>
                </div>
            </div>

        </header>

        <div class="header-empty-space"></div>


        @yield('main')
        @if(\Session::get('poslednji') != null)
        <div class="container">
            <div class="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
            <div class="text-center">
                <div class="simple-article size-3 grey uppercase col-xs-b5">NE ZABORAVITE GDE STE STALI</div>
                <div class="h2">POGLEDALI STE RANIJE</div>
                <div class="title-underline center"><span></span></div>
            </div>
        </div>

        <div class="empty-space col-xs-b20 col-sm-b35 col-md-b70"></div>


        <div class="container">
            <div class="slider-wrapper hidden-pixel-y">
                <div class="swiper-button-prev hidden"></div>
                <div class="swiper-button-next hidden"></div>
                <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="2" data-lt-slides="3"  data-slides-per-view="3">
                    <div class="swiper-wrapper">
                        <?php $i = 0; ?>
                        @foreach(\Session::get('poslednji') as $proizvod)
                            <?php $i++ ?>
                            @if(\Route::currentRouteName() == 'proizvod' && $i == 1)
                                @continue
                            @endif
                            <div class="swiper-slide">
                                <div class="product-shortcode style-5">
                                    <div class="preview">
                                        <div id="cp-sel-Device" style="padding-top:0px !important;">
                                            <div id="cp-device-ori" style="position: relative;">
                                                <div id="cp-mask-img">
                                                    <img alt="" src="{{asset('images/brendovi/' . $proizvod->brend->id . '/' . $proizvod->brend->naziv .'.png') }}">
                                                </div>
                                                <div id="cp-gridme">
                                                    <style>
                                                        .sqr{
                                                            height: 100%;
                                                        }
                                                    </style>
                                                    <div class="container-fluid w-sqr" style="padding-left:0 !important; padding-right:0 !important;">


                                                        <div class="col-md-12 sqr" style="visibility: hidden;">
                                                            <span class="wrap-img-drag no-visible" style="background-image:url('/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg');">
                                                            </span>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="content-animate">
                                                <div class="title">
                                                    <div class="simple-article size-1 color col-xs-b10"><a href="/izaberi_brend_proizvoda?id_brend={{$proizvod->brend->id}}">{{$proizvod->brend->naziv}}</a></div>
                                                    <div class="h6 animate-to-green"><a href="/izaberi_proizvod_i_brend/{{$proizvod->brend->id}}/{{$proizvod->id}}">{{$proizvod->naziv}}</a></div>
                                                </div>
                                                <div class="price">
                                                    <div class="simple-article size-4 dark">
                                                        @if($proizvod->na_popustu)
                                                            <span class="color">{{number_format($proizvod->cena_popust, 0, ',', '.')}} RSD</span>&nbsp;&nbsp;&nbsp;<span class="line-through">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</span>
                                                        @else
                                                            <span class="color">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="preview-buttons">
                                            <div class="buttons-wrapper">
                                                <a class="button size-2 style-2" href="/izaberi_proizvod_i_brend/{{$proizvod->brend->id}}/{{$proizvod->id}}">
                                            
                                                
                                                <span class="text">Pregled</span>
                                            
                                                </a>
                                                <a class="button size-2 style-3" href="javascript:dodajUKorpu('{!! $proizvod->id !!}', '1', '{!! $proizvod->brend->id!!}')">
                                                   
                                                       
                                                        <span class="text">Dodaj u korpu</span>
                                                    
                                                </a>
                                            </div>
                                        </div>

                                    </div>

                            </div>
                            </div>

                        @endforeach
                    </div>
                    <div class="swiper-pagination relative-pagination"></div>
                </div>
            </div>
        </div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>
        @endif

        <!-- FOOTER -->
        <footer>
            <div class="container">
                <div class="footer-top">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <img width="200" src="{{asset('img/logo-beli.png')}}" alt="" />
                            <div class="empty-space col-xs-b20"></div>
                            <div class="simple-article size-2 light fulltransparent">Dizajnirajte svoju masku za telefon. U ponudi imamo maske za sve vrste mobilnih telefona.</div>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-contact"><i class="fa fa-mobile" aria-hidden="true"></i> Telefon: <a href="tel:062 137 96 17">062 137 96 17</a></div>
                            <div class="footer-contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail: <a href="mailto:office@mrcase.rs">office@mrcase.rs</a></div>
                           
                        </div>
                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <h6 class="h6 light">iPhone</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-column-links">
                                
                                <a href="#">iPhone XS Max</a>
                                <a href="#">iPhone XS</a>
                                <a href="#">iPhone 8 Plus</a>
                                <a href="#">iPhone 8</a>
                                <a href="#">iPhone 7 plus</a>
                                <a href="#">iPhone 7</a>
                                <a href="#">iPhone 6/6s</a>
                                    
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <h6 class="h6 light">Samsung</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-column-links">
                                
                                <a href="#">Galaxy S10 Plus</a>
                                <a href="#">Galaxy S10 </a>
                                <a href="#">Galaxy 9</a>
                                <a href="#">Galaxy Note 9</a>
                                <a href="#">Galaxy A7</a>
                                <a href="#">Galaxy Note 8</a>
                                    
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h6 class="h6 light">Tagovi</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="tags clearfix">
                                <a class="tag">maske za telefone</a>
                                <a class="tag">iphone</a>
                                <a class="tag">samsung</a>
                                <a class="tag">maska</a>
                                <a class="tag">drvena maska</a>
                                <a class="tag">dizajn maska</a>
                                <a class="tag">maske sa porukom</a>
                                <a class="tag">premium maske</a>
                                <a class="tag">MR case</a>
                                <a class="tag">huawei</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
                            <div class="copyright">&copy; 2020 Sva prava zadrzana. <a href="/">MR Case</a></div>
                            <div class="follow">
                                <a class="entry" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="entry" target="_blank" href="https://www.instagram.com/mrcase.rs/"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div class="popup-wrapper">
        <div class="bg-layer"></div>

        <div class="popup-content" data-rel="1">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">PRIJVITE SE</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <input class="simple-input" type="text" value="" placeholder="Email" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <input class="simple-input" type="password" value="" placeholder="Lozinka" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <div class="empty-space col-sm-b5"></div>
                            <a class="simple-link">Zaboravili ste lozinku?</a>
                            <div class="empty-space col-xs-b5"></div>
                            <a class="simple-link">Nemate nalog?</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a class="button size-2 style-3" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="{{asset('img/icon-4.png')}}" alt="" /></span>
                                    <span class="text">Prijava</span>
                                </span>
                            </a>  
                        </div>
                    </div>
                    <div class="popup-or">
                        <span>ili</span>
                    </div>
                    <div class="row m5">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <a class="button facebook-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="{{asset('img/icon-4.png')}}" alt="" /></span>
                                    <span class="text">facebook</span>
                                </span>
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a class="button google-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="{{asset('img/icon-4.png')}}" alt="" /></span>
                                    <span class="text">google</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content" data-rel="2">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">register</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <input class="simple-input" type="text" value="" placeholder="Your name" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <input class="simple-input" type="text" value="" placeholder="Your email" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <input class="simple-input" type="password" value="" placeholder="Enter password" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <input class="simple-input" type="password" value="" placeholder="Repeat password" />
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-7 col-xs-b10 col-sm-b0">
                            <div class="empty-space col-sm-b15"></div>
                            <label class="checkbox-entry">
                                <input type="checkbox" /><span><a href="#">Privacy policy agreement</a></span>
                            </label>
                        </div>
                        <div class="col-sm-5 text-right">
                            <a class="button size-2 style-3" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="{{asset('img/icon-4.png')}}" alt="" /></span>
                                    <span class="text">submit</span>
                                </span>
                            </a>  
                        </div>
                    </div>
                    <div class="popup-or">
                        <span>or</span>
                    </div>
                    <div class="row m5">
                        <div class="col-sm-4 col-xs-b10 col-sm-b0">
                            <a class="button facebook-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="{{asset('img/icon-4.png')}}" alt="" /></span>
                                    <span class="text">facebook</span>
                                </span>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a class="button google-button size-2 style-4 block" href="#">
                                <span class="button-wrapper">
                                    <span class="icon"><img src="{{asset('img/icon-4.png')}}" alt="" /></span>
                                    <span class="text">google</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

    </div>

    <script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('js/swiper.jquery.min.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>

    <!-- styled select -->
    <script src="{{asset('js/jquery.sumoselect.min.js')}}"></script>

    <!-- counter -->
    <script src="{{asset('js/jquery.classycountdown.js')}}"></script>
    <script src="{{asset('js/jquery.knob.js')}}"></script>
    <script src="{{asset('js/jquery.throttle.js')}}"></script>

    <!-- masonry -->
    <script src="{{asset('js/isotope.pkgd.min.js')}}"></script>
    <script>
        $(window).load(function(){
            var $container = $('.grid').isotope({
                itemSelector: '.grid-item',
                masonry: {
                    columnWidth: '.grid-sizer'
                }
            });
        });
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    @yield('scriptsBottom')

    <script src="{{asset('js/klijentKorpa.js')}}"></script>
    <script src="{{asset('js/popupDialog.js')}}"></script>

    <script language="JavaScript">
      /**
        * Disable right-click of mouse, F12 key, and save key combinations on page
        * By Arthur Gareginyan (arthurgareginyan@gmail.com)
        * For full source code, visit https://mycyberuniverse.com
        */
      window.onload = function() {
        document.addEventListener("contextmenu", function(e){
          e.preventDefault();
        }, false);
        document.addEventListener("keydown", function(e) {
        //document.onkeydown = function(e) {
          // "I" key
          if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
            disabledEvent(e);
          }
          // "J" key
          if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
            disabledEvent(e);
          }
          // "S" key + macOS
          if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
            disabledEvent(e);
          }
          // "U" key
          if (e.ctrlKey && e.keyCode == 85) {
            disabledEvent(e);
          }
          // "F12" key
          if (event.keyCode == 123) {
            disabledEvent(e);
          }
        }, false);
        function disabledEvent(e){
          if (e.stopPropagation){
            e.stopPropagation();
          } else if (window.event){
            window.event.cancelBubble = true;
          }
          e.preventDefault();
          return false;
        }
      };
    </script>
</body>


</html>
