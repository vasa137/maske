@extends('layout')

@section('title')
PRODAVNICA
@stop

@section('act_prodavnica')
class="active"
@stop


@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/klijentProizvodi.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/prodavnica.js')}}"></script>
@endsection

@section('main')

<div class="container">
    <div class="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
    <div class="row">
        <div class="col-md-9 col-md-push-3">
            <div class="align-inline spacing-1 hidden-xs">
                <a class="pagination toggle-products-view active"><img src="img/icon-14.png" alt="" /><img src="img/icon-15.png" alt="" /></a>
                <a class="pagination toggle-products-view"><img src="img/icon-16.png" alt="" /><img src="img/icon-17.png" alt="" /></a>
            </div>
            <div class="align-inline spacing-1 filtration-cell-width-1">
                <form id="sortirajForma">
                    <select onchange="filtriraj()" name="sort" class="SlectBox small">
                        <option hidden value="null">Sortiraj po</option>
                        <option value="cena-asc">Ceni rastuće</option>
                        <option value="cena-desc">Ceni opadajuće</option>
                        <option value="naziv-asc">Naziv rastuće</option>
                        <option value="naziv-desc">Naziv opadajuće</option>
                    </select>
                </form>
            </div>



            <div class="empty-space col-xs-b25 col-sm-b60"></div>

            <div class="products-content">
                <div class="products-wrapper">
                    <div class="row nopadding" id="proizvodiInclude">
                        @include('include.listaProizvoda')
                    </div>
                    <div class="ajax-load" ></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-pull-9">
            <form id="filterForma">
            <div class="h4 col-xs-b25">Izaberite uređaj</div>
            <select onchange="filtriraj()" style="width: 80%" class="SlectBox" name="brend">
                @foreach($brendovi as $brend)
                    <option value="{{$brend->id}}" @if($brend->id == $izabraniBrend) selected @endif>{{$brend->naziv}}</option>
                @endforeach
            </select>

            <div class="empty-space col-xs-b25 col-sm-b50"></div>

            @foreach($stabloKategorija as $kategorija)
                <div class="h4 col-xs-b25">{{$kategorija->naziv}}</div>
                @if(!empty( $kategorija->children))
                        @include('include.podkategorija', ['kategorije' => $kategorija->children, 'nad_kategorija' => $kategorija])
                @endif
                <div class="empty-space col-xs-b25 col-sm-b50"></div>
            @endforeach



            <!--
           <div class="empty-space col-xs-b25 col-sm-b50"></div>

            <div class="h4 col-xs-b25">Tagovi</div>
            <div class="tags light clearfix">
                <a class="tag">maske za telefone</a>
                <a class="tag">iphone</a>
                <a class="tag">samsung</a>
                <a class="tag">maska</a>
                <a class="tag">drvena maska</a>
                <a class="tag">dizajn maska</a>
                <a class="tag">maske sa porukom</a>
                <a class="tag">premium maske</a>
                <a class="tag">just in case</a>
                <a class="tag">huawei</a>
            </div>

            -->
            </form>
        </div>
    </div>
    
</div>

@stop