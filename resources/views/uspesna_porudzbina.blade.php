@extends('layout')

@section('title')
Uspešna porudžbina
@stop


@section('main')

<div class="container">
    <div class="text-center">
        <div class="empty-space col-xs-b15 col-sm-b30"></div>
        
        <div class="h2">VAŠA PORUDŽBINA JE PRIMLJENA</div>
        <div class="simple-article size-3 grey uppercase col-xs-b5"><br><a href="/"><i class="fa fa-arrow-left"></i> Vratite se na početnu stranu</a></div>
        <div class="title-underline center"><span></span></div>
    </div>
</div>


        
<div class="empty-space col-xs-b35 col-md-b70"></div>

@stop