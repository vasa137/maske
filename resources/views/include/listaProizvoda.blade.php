
@foreach($proizvodi as $proizvod)
    <div class="col-sm-4" style="text-align: center;">
        <div class="product-shortcode style-1" style="padding:0 !important;">
            <div class="title">
                <div class="simple-article size-1 color col-xs-b5">
                    @if(!empty($proizvod->kategorije))
                        @foreach($proizvod->kategorije as $kategorija)
                            {{$kategorija->naziv}}&nbsp;
                            @if($kategorija != $proizvod->kategorije[count($proizvod->kategorije) - 1])
                                |&nbsp;
                            @endif
                        @endforeach
                    @endif
                </div>
                <div class="h6 animate-to-green"><a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">{{$proizvod->naziv}}</a></div>
            </div>
            <div class="preview">
                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">
                <div id="cp-sel-Device" style="padding-top:0px !important;">
                    <div id="cp-device-ori" style="position: relative;">
                        <div id="cp-mask-img">
                            <img alt="" src="{{asset('images/brendovi/' . $proizvod->brend->id . '/' . $proizvod->brend->naziv .'.png') }}">
                        </div>
                        <div id="cp-gridme">
                            <style>
                                .sqr{
                                    height: 100%;
                                }
                            </style>
                            <div class="container-fluid w-sqr" style="padding-left:0 !important; padding-right:0 !important;">


                                <div class="col-md-12 sqr" style="visibility: hidden;">
                                    <span class="wrap-img-drag no-visible" style="background-image:url('/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg');">
                                    </span>

                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                </a>

                <div class="price">
                    <div class="simple-article size-4">
                        @if($proizvod->na_popustu)
                            <span class="color">{{number_format($proizvod->cena_popust, 0, ',', '.')}} RSD</span>&nbsp;&nbsp;&nbsp;<span class="line-through">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</span>
                        @else
                            <span class="color">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</span>
                        @endif
                    </div>
                </div>
            </div>

            <!--
            <div class="description">
                <div class="icons">
                    <a class="entry"><i class="fa fa-check" aria-hidden="true"></i></a>

                    <a class="entry"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
            </div>
            -->
        </div>
    </div>
@endforeach