<div @if(isset($dialogId)) id="dialog-{{$dialogId}}" @endif class="cd-popup" role="alert">
    <div class="cd-popup-container">
        <p>{!! $poruka !!}</p>
        <ul class="cd-buttons">
            <li><a href="{!! $linkUspesno !!}">@if(!isset($yesButtonText)) Da @else {{$yesButtonText}} @endif</a></li>
            @if(isset($dialogId))
                <li><a href="javascript:zatvoriDialogSaId('{!! $dialogId !!}');">@if(!isset($noButtonText)) Ne @else {{$noButtonText}} @endif</a></li>
            @else
                <li><a href="javascript:zatvoriDialog();">@if(!isset($noButtonText)) Ne @else {{$noButtonText}} @endif</a></li>
            @endif
        </ul>

        @if(isset($dialogId))
            <a href="javascript:zatvoriDialogSaId('{!! $dialogId !!}');" class="cd-popup-close img-replace"></a>
        @else
            <a href="javascript:zatvoriDialog();" class="cd-popup-close img-replace"></a>
        @endif
    </div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->
<!-- banner Area Start -->