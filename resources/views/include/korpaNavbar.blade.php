@foreach(Cart::instance('korpa')->content() as $stavka)
    @include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da obrišete proizvod iz korpe?', 'linkUspesno' => 'javascript:obrisiIzKorpe(\''. $stavka->rowId . '\',\'' . explode('_',$stavka->id)[0] .'\')', 'dialogId' => $stavka->rowId])
@endforeach
<a href="/korpa">
    <b class="hidden-xs">KORPA</b>
    @if(Route::getCurrentRoute()->getName() != 'pregled_korpe' && Route::getCurrentRoute()->getName() != 'naplati')
    <span class="cart-icon">
        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
        <span class="cart-label">{{Cart::instance('korpa')->count()}}</span>
    </span>
    <span class="cart-title hidden-xs">{{Cart::instance('korpa')->total(0,',', '.')}} rsd</span>
    @endif
</a>
@if(Route::getCurrentRoute()->getName() != 'pregled_korpe'  && Route::getCurrentRoute()->getName() != 'naplati')
<div class="cart-toggle hidden-xs hidden-sm cart">
    @if(Cart::instance('korpa')->count() == 0)
        Korpa je prazna.
    @else

    <div class="cart-overflow">
        @foreach(Cart::instance('korpa')->content() as $stavka)

        <div class="cart-entry clearfix">
            @if(intval(explode('_',$stavka->id)[0]) > 0)
            <a class="cart-entry-thumbnail" href="/proizvod/<?=str_replace('?', '',str_replace(' ', '_', $stavka->name))?>/{{explode('_',$stavka->id)[0]}}">
                <div id="cp-sel-Device2" style="padding-top:0px !important;">
                    <div id="cp-device-ori2" style="position: relative; width:45px; height:auto;">
                        <div id="cp-mask-img">
                            <img  alt="" src="{{asset('images/brendovi/' . explode('_',$stavka->id)[1] . '/' . \App\Brend::dohvatiSaId(explode('_',$stavka->id)[1])->naziv .'.png') }}">
                        </div>
                        <div id="cp-gridme">
                            <style>
                                .sqr{
                                    height: 100%;
                                }
                            </style>
                            <div class="container-fluid w-sqr" style="padding-left:0 !important; padding-right:0 !important;">


                                <div class="col-md-12 sqr" style="visibility: hidden;">
                                                    <span class="wrap-img-drag no-visible" style="background-image:url('{{asset('/images/proizvodi/'. explode('_',$stavka->id)[0] .'/glavna/' . $stavka->nazivGlavneSlike . '.jpg')}}');">
                                                    </span>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </a>
            @else
                <a class="cart-entry-thumbnail" href="javascript:void(0)">
                @if(File::exists(public_path('/images/maske/' . \Session::getId() . '/' . explode('_',$stavka->id)[0]  .  '/maska.png')))
                    <div id="cp-sel-Device2" style="padding-top:0px !important;">
                        <div id="cp-device-ori2" style="position: relative; width:45px; height:auto;">
                            <div id="cp-mask-img">
                                <img  alt="" src="{{asset('images/brendovi/' . explode('_',$stavka->id)[1] . '/' . \App\Brend::dohvatiSaId(explode('_',$stavka->id)[1])->naziv .'.png') }}">
                            </div>
                            <div id="cp-gridme">
                                <style>
                                    .sqr{
                                        height: 100%;
                                    }
                                </style>
                                <div class="container-fluid w-sqr" style="padding-left:0 !important; padding-right:0 !important;">


                                    <div class="col-md-12 sqr" style="visibility: hidden;">
                                                <span class="wrap-img-drag no-visible" style="background-image:url('{{asset('/images/maske/' . \Session::getId() . '/' . explode('_',$stavka->id)[0]  .  '/maska.png')}}');">
                                                </span>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                @endif
                </a>
            @endif
            <div class="cart-entry-description">
                <table>
                    <tr>
                        <td>
                            <div class="h6">
                                @if(intval(explode('_',$stavka->id)[0]) > 0)
                                    <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '_', $stavka->name))?>/{{explode('_',$stavka->id)[0]}}">
                                        {{$stavka->brend->naziv}} - {{$stavka->name}}
                                    </a>
                                @else
                                    {{$stavka->brend->naziv}} - Samostalni dizajn
                                @endif
                            </div>
                            <div class="simple-article size-1">KOLIČINA: {{$stavka->qty}}</div>
                        </td>
                        <td>
                            <div class="simple-article size-3 grey">{{number_format($stavka->price - $stavka->tax, 0, ',', '.')}} rsd</div>
                            <div class="simple-article size-1">UKUPNO: {{$stavka->total(0, ',', '.')}} rsd</div>
                        </td>
                        <td>
                            <div class="button-close" onclick="otvoriDialogSaId('{!! $stavka->rowId!!}')"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    <div class="empty-space col-xs-b40"></div>
    <div class="row">
        <div class="col-xs-6">
            <div class="cell-view empty-space col-xs-b50">
                <div class="simple-article size-5 grey">UKUPNO <span class="color">{{Cart::instance('korpa')->total(0,',', '.')}} rsd</span></div>
            </div>
        </div>
        <div class="col-xs-6 text-right">
            <a class="button size-2 style-3" href="/korpa">
                <span class="button-wrapper">
                    <span class="icon"><img src="{{asset('img/icon-4.png')}}" alt=""></span>
                    <span class="text">PREGLED KORPE</span>
                </span>
            </a>
        </div>
    </div>

    @endif
</div>
@endif