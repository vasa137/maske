<div id="tabela-vauceri">
<h1>Vaučeri</h1>
    <br/><br/>
<table class="cart-table" >

    <thead>
    <tr>
        <th></th>
        <th>Kod vaučera</th>
        <th>Iznos</th>
        <th style="width: 70px;"></th>
    </tr>
    </thead>
    <tbody>
        @foreach($stavkeVauceri as $stavkaVaucer)
            <tr id="stavka-vaucer-{{$stavkaVaucer->rowId}}">
                @include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da uklonite vaučer?', 'linkUspesno' => 'javascript:ukloniVaucer(\''. $stavkaVaucer->rowId . '\',\'' . $stavkaVaucer->id .'\')', 'dialogId' => 'vaucer-' . $stavkaVaucer->rowId])
                <td data-title="Tip kupona" >
                    Vaučer
                </td>
                <td data-title="Naziv" >
                    <div class="price">{{$stavkaVaucer->name}}</div>
                </td>
                <td data-title="Iznos" >
                    <div class="price">-{{number_format($stavkaVaucer->price, 0, ',', '.')}} rsd</div>
                </td>
                <td data-title="Akcija">
                    <div class="button-close" onclick="otvoriDialogSaId('vaucer-{!! $stavkaVaucer->rowId!!}')"></div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
