@extends('layout')

@section('title')
INSTA SHOP
@stop

@section('act_insta')
class="active"
@stop



@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/klijentProizvodi.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/prodavnica.js')}}"></script>
@endsection


@section('main')

<div class="container">
    <div class="text-center">
        <div class="empty-space col-xs-b15 col-sm-b30"></div>
        <div class="simple-article size-3 grey uppercase col-xs-b5">PORUČITE BILO KOJI DIZAJN SA NAŠEG INSTAGRAM PROFILA ZA SVE MODELE MOBILNIH TELEFONA</div>
        <div class="h2">INSTA SHOP</div>
        <div class="title-underline center"><span></span></div>
    </div>
</div>
<div class="row nopadding">
    @foreach($proizvodi as $p)
    <div class="col-md-3">
        <a href="{{$p->kratak_opis}}">
         <img width="100%"  src="http://mrcase.rs/images/proizvodi/{{$p->id}}/glavna/{{$p->nazivGlavneSlike}}.jpg">
        </a>
    </div>
    @endforeach
    
</div>

        
<div class="empty-space col-xs-b35 col-md-b70"></div>

@stop
