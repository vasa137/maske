function prikaziDostupne(){
    $('#tabela-clanci-obrisani').css('display', 'none');
    $('#tabela-clanci-aktivni').css('display', 'block');
    $('#tabela-clanci-obrisani_wrapper').css('display', 'none');
    $('#tabela-clanci-aktivni_wrapper').css('display', 'block');

    $('#clanci-title').html('Dostupni članci');
}

function prikaziNedostupne(){
    $('#tabela-clanci-obrisani').css('display', 'block');
    $('#tabela-clanci-aktivni').css('display', 'none');
    $('#tabela-clanci-obrisani_wrapper').css('display', 'block');
    $('#tabela-clanci-aktivni_wrapper').css('display', 'none');

    $('#clanci-title').html('Obrisani članci');
}