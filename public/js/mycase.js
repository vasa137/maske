/*
 * jQuery Mycase Plugin
 * @version 1.0
 *
 * Example Usage:
 * $('#cp-wrap-content').Mycase({
 *    	path_site:'', //Path To Theme
		easing:'easeInOutQuad' //Custom Transition
 * });
 */
'use strict';

(function($){
	/*== Plugin Constructor ==*/
	var Mycase=function(selector,options){
		this.$elem=$(selector);
		this.$options=options;
	}

	/*== Plugin Prototype ==*/
	Mycase.prototype={
		defaults:{
			path_site:'', //Path To Theme
			easing:'easeInOutQuad' //Custom Transition
		},

		// Init Plugin
		init:function(){
			this.setting=$.extend({}, this.defaults, this.options);
			this.loadDevice();  //Call Function Load Device 
			this.chooseDevice(); //Call Function Choose Device 
			this.loadLayout(); //Call Function Load Layout 
			this.chooseLayout(); //Call Function Choose Layout 
			this.putImageToDevice(); //Call Function Put Image Into Device 
			this.getShuffleImg(); //Call Function Shuffle Image
			this.resetImg(); //Call Function Shuffle Image
			this.uploadOtherImg(); //Call Function Upload New Image
			this.putTextDevice(); //Call Function Put Text Into Device
			this.cropImg(); //Call Function Crop Image
			this.deleteImg(); //Call Function Delete Image From Device
			this.getCanvasDevice(); //Call Function Get Canvas From Html Code
			this.animateSteps(); //Call Function Animate step
			this.chooseModel();  //Call Function Choose Model
		},

		// Select Device
		chooseDevice:function(){
			var self=this;
			$(document).on('click','.cp-choose-dev-button',function(e){
				var $name_img_mask=$(this).find('span.img-find-mask').text();
				$('.cp-model li').hide();
				/*
				var $clear = $(this).find('#casetypeclear').text();
				var $white = $(this).find('#casetypewhite').text();
				var $black = $(this).find('#casetypeblack').text();
				var $tpu = $(this).find('#casetypetpu').text();
				*/
				/*
				$('.casetypeclear,.casetypeblack,.casetypewhite,.casetypetpu').show();
				*/
				var $tpu = $(this).find('#casetypetpu').text();
				self.$elem.find('.cp-title').text($(this).find('.tit-prod').text());

				self.$elem.find('.cp-token').val( $(this).attr('id'));
				self.getLoader();
				let nazivBrenda = $(this).find('.tit-prod').text();
				let idBrend = $(this).find('.idBrend')[0].value;
				self.$elem.find('#cp-mask-img').children('img').attr('src','/images/brendovi/' +idBrend + '/' + nazivBrenda + '.png');
				self.$elem.find('.cp-device a').removeClass('active');
				$(this).addClass('active');
				//self.$elem.find('#cp-mask-img').children('img').load(function(){
					self.$elem.find('.step1 .list').slideUp('slow');
					self.$elem.find('.step1').removeClass('actived-bl');
					self.$elem.find('.step2').addClass('actived-bl');
					self.$elem.find('.step2').removeClass('disabled');
					self.$elem.find('.step2 .list').stop().slideDown('slow');
					self.$elem.find('.step3').addClass('actived-bl');
					self.$elem.find('.step3').removeClass('disabled');
					$('.cp-loader').fadeOut('fast', function() {
						$('.cp-loader').remove();
					});
				//});
				$('.cp-btn-action,.cp-btn-save').fadeIn('slow');
				$('.cp-btn-save').css('visibility', 'visible');
				return true;
			});
		},

		// Select Model
		chooseModel:function(){
			var self=this;
			$(document).on('click','.cp-model a',function(e){
				self.$elem.find('.cp-model a').removeClass('active');
				jQuery(this).addClass('active');
				self.$elem.find('.step2 .list').slideUp('pretty');
				self.$elem.find('.step3').removeClass('disabled').find('.list').slideDown('pretty');
				self.$elem.find('.step4').removeClass('disabled');
				return false;
			});
		},

		// Select Layout
		chooseLayout:function(){
			var self=this;
			$(document).on('click','.cp-choose-grid-button',function(e){
				var $name_txt_grid=$(this).find('span.title-find').text();
				self.$elem.find('#cp-device-ori').append(self.getLoader());
				self.$elem.find('#cp-gridme').html(' ');
				self.$elem.find('#cp-sel-layout a').removeClass('active');
				jQuery(this).addClass('active');
				$.ajax({
					url: 'config/layout/g_1/grid.html',
					context: document.body
				}).done(function(data) {
					self.$elem.find('#cp-gridme,#cp-gridme-cover').html(data);
					self.$elem.find('.cp-loader').fadeOut('pretty',function(){
			       		self.$elem.find('.cp-loader').remove();
			        });
			        $('.cp-btn-action,.cp-btn-save').fadeIn('slow');
					$('.cp-btn-save').css('visibility', 'visible');
					self.$elem.find('.step3 .list').slideUp('pretty');
					self.$elem.find('.step4').removeClass('disabled').find('.list').slideDown('pretty');
					self.$elem.find('.step5').removeClass('disabled');
				});

				e.preventDefault();

			});

		},



		// Load Device
		loadDevice:function(){
			
			// Call Data From Apple Json File
			this.loadDeviceFromJson(this.setting.path_site+'config/device/data_apple.json','.step1 .cp-device');

		},

		// Load Data Device From Json File
		loadDeviceFromJson:function(path_file,selector_bloc){
			var self=this;
			//$.getJSON(path_file, function(data) {
				var $html_tags='';
				var $i=0;
				//$.each(data.d, function(index, val) {
				for(let i = 0; i < listaBrendova.length; i++){
					$html_tags='<li><a href="javascript:void($(\'#izabraniBrend\').val(' + listaBrendova[i]['id'] + '))" class="cp-choose-dev-button"><span class="tit-prod">'+ listaBrendova[i]['naziv'] +'</span><input type="hidden" class="idBrend" value="' +listaBrendova[i]['id'] +  '"/> </a></li>';
					$(selector_bloc).append($html_tags);
				}

				//});
			//});
		},

		// Load Layout From Json
		loadLayout:function(){
			var self=this;
			/*
			$.getJSON(self.setting.path_site+'config/layout/data.json', function(data) {
				var $html_tags='';
				$.each(data.d, function(index,val) {
					 $html_tags='<li><a href="" class="cp-choose-grid-button"><img src="'+self.setting.path_site+'config/layout/'+val+'/grid.png"/><span class="title-find" style="display:none">'+val+'</span></a></li>';
					 self.$elem.find('#cp-sel-layout').children('ul').append($html_tags);
				});
			});
			*/
		},

		// Upload Other Image
		uploadOtherImg:function(){

			var url = 'upload_case_image'; //url to upload image
			var self=this;
		    $('#fileupload').fileupload({
		        url: url,
		        add: function (e, data) {

		            var runUpload = true;
			        var uploadFile = data.files[0];
			        if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
			            alertify.alert("You must select an image file only");
			            runUpload = false;
			        }
			        else if (uploadFile.size > 100000000) {
			        	alertify.alert("Please upload a smaller image, max size is 100 MB");
			            runUpload = false;
			        }
			        if (runUpload == true) {
			        	//$('#cp-img-lo-w').attr('style', 'display: flex !important');
		           	    $('.cp-loader-number').text(' ');
			            data.submit();

			        }
	        	},
		        done: function (e, data) {

		            //$.each(data.result.files, function (index, file) {
		            	$('#cp-sel-Photos ul').append('<li style="background-image:url('+data.result+');"><a href="'+data.result+'" style="background-image:url('+data.result+');"><i class="fa fa-plus"></i></a></li>');
		            //});
		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('.cp-loader-number').text(progress + '%');
		        },
		        always:function(){
		        },
		        stop:function(){
		        	setTimeout(function(){
						//$('#cp-img-lo-w').attr('style', 'display: none !important');
		        		$('.cp-loader-number').text(' ');
		            },1000);
		        }
		    });
		},
		putImageToDevice:function(){

			var $is_fill='';

			var $inc=0;

			var self=this;

			var src_img;

			var $this;


			//Add Image To Row

			$(document).on('click','#cp-sel-Photos ul li a', function(event) {
				$inc=0;

				$this=$(this);

				self.$elem.find('#cp-gridme *').remove();

				if( self.$elem.find('.cp-cibled-row')[0] )

				{
					self.$elem.find('.cp-cibled-row').html('<span class="wrap-img-drag" style="background-image:url('+$this.attr("href")+');"><img src="'+$this.attr("href")+'" alt=""/><samp class="cp-h-v"><a href="javascript:void(0);" class="fa-stack fa-lg cp-modify" title="Crop this picture"><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></a><a  title="Delete this picture" href="javascrip:void(0);" class="fa-stack fa-lg cp-delete-fast"><i class="fa fa-trash-o"></i></a></samp></span>');

					self.$elem.find('.sqr').css('visibility','visible');

					self.$elem.find('.wrap-img-drag').addClass('no-visible');

					self.$elem.find('.sqr').css('visibility','hidden');

					self.$elem.find('.sqr').removeClass('cp-cibled-row');

					self.copyHtmlGrid();

				}

				else if( self.$elem.find('.sqr')[0] )

				{
					self.$elem.find('.sqr').css('visibility','visible');

					self.$elem.find('.sqr').each(function(index, val) {

						$is_fill=$.trim($(this).html());

						//if( ( !$is_fill || $is_fill=='undefined' || $is_fill==''  )  && $inc==0 )

						//{

							var $this_e=$(this);

							$(this).html('<span class="wrap-img-drag" style="background-image:url('+$this.attr("href")+');"><img src="'+$this.attr("href")+'" alt=""/><samp class="cp-h-v"><a href="javascript:void(0);" class="fa-stack fa-lg cp-modify"  title="Crop this picture"><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></a><a href="javascript:void(0);" class="fa-stack fa-lg cp-delete-fast" title="Delete this picture"><i class="fa fa-trash-o"></i></a></samp></span>');

							$this_e.find('.wrap-img-drag').addClass('no-visible');

							self.$elem.find('.sqr').css('visibility','hidden');

							$(this).removeClass('cp-cibled-row');

							$inc+=1;

						//}

					});

					self.copyHtmlGrid();

				}

				else{

					alertify.alert("You must select the layout");

				}

				return false;

			});



			self.$elem.find('#cp-sel-Photos ul li a').each(function(index, val) {

				src_img=$(this).children('img').attr('src');

				$(this).attr('href',src_img).css('background-image','url('+src_img+')');

			});



			//Select Row

			$(document).on('click','.sqr', function(e) {

				var $txt=$.trim($(this).html());

				if( !$(this).hasClass('cp-cibled-row') ) self.$elem.find('.sqr').removeClass('cp-cibled-row');

				$(this).toggleClass('cp-cibled-row');

				self.copyHtmlGrid();

				e.preventDefault();

			});



		},

		// Delete Image
		deleteImg:function(){
			var self=this;
			$(document).on('click','#cp-btn-delete',function(e){
				alertify.confirm("Are you sure you want to remove this picture ?", function (e) {
					if (e) {
						self.$elem.find('#cp-sel-Text').show();
						$('.cp-wpreviw').closest('span').remove();
						self.$elem.find('#cp-crop-image').hide();
						self.$elem.find('#cp-cropit-me *').remove();
						self.copyHtmlGrid();
					}
				});
				e.preventDefault();
			});
			$(document).on('click','.cp-delete-fast',function(e){
				$(this).closest('span').remove();
				self.copyHtmlGrid();
				e.preventDefault();
			});
		},
		// Crop Image
		cropImg:function(){
			var one;
			var self=this;
			$(document).on('click','#cp-gridme-cover .cp-modify',function(e){
				self.getLoader();
				$(this).closest('.wrap-img-drag').find('img').wrap('<samp class="cp-wpreviw preview"><samp>');
				var $img=$(this).closest('.wrap-img-drag').find('img').attr('src');
				self.$elem.find('#cp-cropit-me').html('<ul id="cp-nav-r"><li><a id="cp-btn-delete" href="javascript:void(0);">Trash</a></li><li><a class="cp-btn-cancel" href="javascript:void(0);">Cancel</a></li><li><a class="cl-validate-crop" href="javascript:void(0);">OK</a></li></ul>\
														<div class="eg-wrapper">\
													       <img class="cropper" src="'+$img+'" alt="">\
													    </div>\
													    <div class="eg-button">\
													        <a href="javascript:void(0);" id="reset" title="reset"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-3x"></i><i class="fa fa-refresh fa-stack-2x"></i></span></a>\
													        <a href="javascript:void(0);" id="zoomIn" title="zoom in"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-3x"></i><i class="fa fa-search-plus fa-stack-2x"></i></span></a>\
													        <a href="javascript:void(0);" id="zoomOut" title="zoom out"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-3x"></i><i class="fa fa-search-minus fa-stack-2x"></i></span></a>\
													        <!--a href="javascript:void(0);" id="rotateLeft" title="rotate left"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-3x"></i><i class="fa fa-undo fa-stack-2x"></i></span></a>\
													        <a href="javascript:void(0);" id="rotateRight" title="rotate right"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-3x"></i><i class="fa fa-repeat fa-stack-2x"></i></span></a-->\
													    </div>\
													    <div class="cp-preview" id="cp-main"><div id="cp-device-ori-prev"></div></div>');
				self.$elem.find('.cp-preview #cp-device-ori-prev').html($('#cp-device-ori').html());
				$(' #cp-device-ori-prev #cp-gridme').html( $(' #cp-device-ori-prev #cp-gridme-cover').html());
				var $cropper = $(".cropper"),
				console = window.console || {log:$.noop},
				cropper;
				$cropper.cropper({
				aspectRatio: 9 / 16,
				data: {
				  x: 150,
				  y: 50,
				  width: 640,
				  height: 360
				},
				preview: ".preview",
					viewMode:3
				});
				cropper = $cropper.data("cropper");
				$("#reset").unbind('click').bind('click',function() {
					$cropper.cropper("reset");
				});
				$("#zoomIn").unbind('click').bind('click',function() {
					$cropper.cropper("zoom", 0.2);
				});
				$("#zoomOut").unbind('click').bind('click',function() {
					$cropper.cropper("zoom", -0.2);
				});
				$("#rotateLeft").unbind('click').bind('click',function() {
					$cropper.cropper("rotate", -90);
				});
				$("#rotateRight").unbind('click').bind('click',function() {
					$cropper.cropper("rotate", 90);
				});
				setTimeout(function(){
					self.$elem.find('#cp-crop-image').fadeIn('slow');
					self.$elem.find('#cp-sel-Text').hide();
					$('.cp-loader').remove();
				},1300);
				return false;
			});

			$(document).on('click', '.cl-validate-crop', function() {
				self.$elem.find('#cp-sel-Text').show();
				$('.cp-wpreviw').closest('span').css('background-image','none');
				$('.cp-wpreviw img').addClass('cp-show-a-crop');
				$('.cp-wpreviw img').unwrap();
				self.copyHtmlGrid();
				self.$elem.find('#cp-crop-image').hide();
				self.$elem.find('#cp-cropit-me *').remove();
				return false;
			});  
			
			$(document).on('click', '.cp-btn-cancel', function() {
				self.$elem.find('#cp-sel-Text').show();
				$('.cp-wpreviw img').unwrap();
				self.$elem.find('#cp-crop-image').hide();
				self.$elem.find('#cp-cropit-me *').remove();
				return false;
			});
		},
		// Process Step Text 
		getText:function(){
			var $val_txt=$.trim($('.cp-input-txt').val());
			var $font=$('.cp-list-font a.active').attr('id');
    		var $color=$('.cp-active-color-t').attr('id');
    		var $taille=$('#cp-size-txt .active').attr('id');
    		var $classes=$font+' '+$color+' '+$taille;
    		$('#cp-input-gen').attr('class',$classes);
    		$('#cp-input-gen').text($val_txt);
		},
		// Put Text Into Device
		putTextDevice:function(){
			var self=this;
			$('.cp-input-txt').keyup(function(){
				self.getText();
			});
			$('#cp-input-gen').pep({
		          constrainTo: 'parent',
		          useCSSTranslation: false
			});

	        $(document).on('click','.cp-list-font a',function(){
	    		$('.cp-list-font a').removeClass('active');
	    		$(this).addClass('active');
	    		self.getText();
	    		return false;
	    	});
	    	$(document).on('click','.cp-list-color a',function(){
	    		$('.cp-list-color a').removeClass('cp-active-color-t');
	    		$(this).addClass('cp-active-color-t');
	    		self.getText();
	    		return false;
	    	});
	    	$(document).on('click','#cp-size-txt a',function(){
	    		$('#cp-size-txt a').removeClass('active');
	    		$(this).addClass('active');
	    		self.getText();
	    		return false;
	    	});
		},
		// Copy Code From Grid
		copyHtmlGrid:function(){
			this.$elem.find('#cp-gridme').html(this.$elem.find('#cp-gridme-cover').html());
		},
		// Get Canvas Device From Step One
		getCanvasDevice:function(){
			var self=this;
			$(document).on('click','.cp-btn-save',function(e){

				var $size_row=$('#cp-gridme .sqr').size();
				var $size_img=$('#cp-gridme .wrap-img-drag').size();
				if( $size_row == $size_img )
				{
					$('.overlay-bg').height($(document).height());
					$('html,body').animate({scrollTop:0},500);
					$('.loader-canvas .progress').width('0%');
					$('.loader-canvas .progress-txt').text('0%');
					$('.overlay-bg').fadeIn('pretty',function(){

						$('#cp-gridme .sqr .wrap-img-drag img').show();
						$('#cp-device-ori').addClass('no-bg-gen');
						$('body').append( '<div id="cp-device-ori-to-print"><div id="cp-device-ori" class="no-phone no-bg-gen">'  + $('#cp-device-ori').html() + '</div></div>');
						$('#cp-device-ori-to-print #cp-device-ori').width( $('#cp-sel-Device #cp-device-ori').width() * 2 );
						self.centerImgBigReso();
						self.centerImg();
						$('html,body').animate({scrollTop:0},0);
						$('#cp-sel-Device').append('<div id="cp-device-ori-gen"><div id="cp-device-ori" class="no-bg-gen"></div></div>');
						$('#cp-device-ori-gen #cp-device-ori').html( $('#cp-sel-Device>#cp-device-ori').html() );
						if (!Date.now) {
						    Date.now = function() { return new Date().getTime(); }
						}

						var $date_now = Date.now();
						html2canvas([document.getElementById('cp-device-ori-gen')], {
							proxy:'https://html2canvas.appspot.com/query',
						    onrendered: function(canvas) {
						    	$('#cp-device-ori-to-print').css('z-index','1000');
						    	self.createImgToPrint('cp-device-ori-to-print','',$date_now);

						    } 
						});
					});
				}
				else
				{
					alertify.alert("You must fill all grids before you can generate the canvas");
				}
				e.preventDefault();
			});
			
		},

		createImgToPrint:function(id,url_img,$date_now,ipa,idp){

			var self=this;
			$('html,body').animate({scrollTop:0},1);
			html2canvas([document.getElementById(id)], {
				proxy:'https://html2canvas.appspot.com/query',
			    onrendered: function(canvas) {
			    	//document.body.appendChild(canvas0);
			    	$('#cp-device-ori-gen,#cp-device-ori-to-print').remove();
			    	var url_to_print = canvas.toDataURL("image/png");
					$.ajax({
					  xhr: function()
					  {
					    var xhr = new window.XMLHttpRequest();
					    xhr.upload.addEventListener("progress", function(evt){
					      if (evt.lengthComputable) {
					        var percentComplete = (evt.loaded / evt.total) * 100;
					         $('.loader-canvas .progress').width(percentComplete+'%');
					         $('.loader-canvas .progress-txt').text(parseInt(percentComplete)+'%');
					      }
					    }, false);
					    return xhr;
					  },
					  type: 'POST',
					  url: self.setting.path_site+'upload_final_case_image',
					  data: {
					  	id_img : $date_now,
						img_to_print : url_to_print
					  },
					  success: function(data){
			    		$('#cp-gridme .sqr .wrap-img-drag img').hide();
						$('#cp-device-ori').removeClass('no-bg-gen');
						$('#cp-input-gen').html('');
						self.$elem.find('.sqr *').remove();
						self.$elem.find('.sqr').removeClass('cp-cibled-row');
						$('.overlay-bg').fadeOut('pretty');
						  var url = canvas.toDataURL("image/png");

						  $.post( "/dodaj_u_korpu?id_proizvod=0&kolicina=1&id_brend=" + $('#izabraniBrend').val() , function( data ) {
							  otvoriDialog();

							  if(document.getElementById('korpaInclude') !== undefined) {
								  $korpaInclude.html(data.html);
							  }
						  }).fail(function(jqXHR, ajaxOptions, thrownError)
						  {
							  alert('server nije dostupan...');
						  });

						/*
						alertify.alert('Images successfully generated at your server : <br /><br /><br /><img src="'+self.setting.path_site+data+'.png" width="120px" />&nbsp;&nbsp;&nbsp;<img src="'+self.setting.path_site+data+'_.png" width="120px" /><br /><br /><br />');
						 */
					  }
					});
			    }
			
			});
		},

		// Animate Step
		animateSteps:function($index){
			this.$elem.find('.btn-open').bind('click', function(event) {

				if( !$(this).closest('.disabled') [0] && !$(this).closest('.active-bl')[0] )
				{
					$('.step .list').slideUp('slow');
					$('.step').removeClass('actived-bl');
					$(this).parent().addClass('actived-bl');
					$(this).next('.list').stop().slideDown('slow');
				}
			});
		},

		// Align Image On Grid
		centerImg:function(){
			this.$elem.find('.wrap-img-drag').each(function(index,val) {
				if( ! $(this).children('img').hasClass('cp-show-a-crop') )
				{
					$(this).children('img').attr('id','cp_img_'+index);
					imgCoverEffect(document.getElementById('cp_img_'+index), {
					  alignX: 'center',
					  alignY: 'middle' // suppose the function explicitly called on parent resize 
					});
				}
			});
		},
		// Align Image On Grid
		centerImgGen:function(){
			$('#cp-device-ori-gen #cp-gridme .wrap-img-drag').each(function(index,val) {
				
				if( ! $(this).children('img').hasClass('cp-show-a-crop') )
				{
					$(this).children('img').attr('id','cp_img_'+index);
					imgCoverEffect(document.getElementById('cp_img_'+index), {
					  alignX: 'center',
					  alignY: 'middle' // suppose the function explicitly called on parent resize 
					});
				}
			});
		},

		centerImgBigReso:function(){

			$('#cp-device-ori-to-print .wrap-img-drag').each(function(index,val) {

				if( ! $(this).children('img').hasClass('cp-show-a-crop') )

				{

					$(this).children('img').attr('id','cp_img_'+index);

					imgCoverEffect(document.getElementById('cp_img_'+index), {

					  alignX: 'center',

					  alignY: 'middle'

					});

				}

			});

			var $elem = $('#cp-device-ori-to-print  #cp-input-gen');

			var left = $elem.position().left * 2;

			var top = $elem.position().top * 2;

			var font = parseInt($elem.css('font-size')) * 2;

			$('#cp-device-ori-to-print .cp-show-a-crop').each(function(){
				 	var height = $(this).height() * 2;
				 	var width = $(this).width() * 2;
				 	var marginLeft = parseInt( $(this).css('margin-left') ) * 2;
				    var marginTop = parseInt( $(this).css('margin-top') ) * 2;
				    $(this).css({
				    	'width' : width + 'px',
				    	'height' : height + 'px',
				    	'margin-left' : marginLeft + 'px',
				    	'margin-top' : marginTop + 'px',
				    });
			});

			$('#cp-device-ori-to-print  #cp-input-gen').css({'top':top +'px' , 'left':left +'px', 'font-size':font +'px'});

		},
		// Reset Data
		resetImg:function(){
			var self=this;
			$(document).on('click','.cp-btn-reset',function(e){
				if( $('.wrap-img-drag')[0] )
				{
					alertify.confirm("Are you sure you want remove all pictures ?", function (e) {
						if (e) {
							self.$elem.find('.sqr *').remove();
							self.$elem.find('.sqr').removeClass('cp-cibled-row');
						}
					});
				}
				e.preventDefault();
			});
		},

		// Shuffle Image
		getShuffleImg:function(){
			var $tab_u_img;
			var self=this;
			$(document).on('click','.cp-btn-shuffle',function(e){
				self.$elem.find('#cp-gridme *').remove();
				self.$elem.find('.sqr *').remove();
				self.$elem.find('.sqr').css('visibility','visible');
				$tab_u_img=[''];
				self.$elem.find('#cp-sel-Photos ul li a').each(function(index, val) {
					$tab_u_img[index]=$(this).attr('href');
				});
				self.$elem.find('.cp-cibled-row').removeClass('cp-cibled-row');
				$tab_u_img=self.shuffleData($tab_u_img);
				var $inc=0;
				self.$elem.find('.sqr').each(function(index, val) {
					var $this_e=$(this);
					if($inc==$tab_u_img.length) { 
						$inc=0;
						$tab_u_img=self.shuffleData($tab_u_img);
					}
					$(this).html('<span class="wrap-img-drag" style="background-image:url('+$tab_u_img[$inc]+');"><img src="'+$tab_u_img[$inc]+'" alt=""/><samp class="cp-h-v"><a href="javascript:void(0);" class="fa-stack fa-lg cp-modify"  title="Crop this picture"><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></a><a  title="Delete this picture" href="javascrip:void(0);" class="fa-stack fa-lg cp-delete-fast"><i class="fa fa-trash-o"></i></a></samp></span>');
					$inc++;
					$this_e.find('.wrap-img-drag').addClass('no-visible');
					self.$elem.find('.sqr').css('visibility','hidden');
				});
				self.copyHtmlGrid();
				e.preventDefault();
			});
		},

		// Shuffle Data
		shuffleData:function(o) {
			for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
		},

		// Get Loader
		getLoader:function(){
			this.$elem.append('<div class="cp-loader" style="position:absolute;left:0;top:0;right:0;bottom:0;z-index:90000;"><div style="position:absolute;left:0;top:0;bottom:0;right:0;background:#fff; opacity:0;filter:alpha(opacity=0);"></div><i class="fa fa-spinner fa-spin" style="position:absolute;top:50%;left:50%;margin:-30px 0 0 -20px;color:#3ad6e4;font-size:45px;"></i></div>');
		},

		// Select Title Of Step
		getActiveTitle:function(index){
			var $tag_li=this.$elem.find('#cp-title-step').children('li');
			$tag_li.removeClass('cp-show');
			$tag_li.eq(index).addClass('cp-show');
		},

		// Active Link To Step
		getActiveLinkStep:function(index){
			this.$elem.find('#cp-link-step').children('li').eq(index).addClass('cp-active');
		},

		// Current Link To Step
		getCurrentLink:function(index){
			var $tag_li=this.$elem.find('#cp-link-step').children('li');
			$tag_li.removeClass('cp-current-step');
			$tag_li.eq(index).addClass('cp-current-step');
		},

		// Custom Transition
		easing:function(){
			$.easing.custom = function (x, t, b, c, d) { 
			var s = 1.70158;  
			if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b; 
			return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
			}
			jQuery.extend( jQuery.easing,
			{
				def: 'easeOutQuad',
				swing: function (x, t, b, c, d) {
					return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
				},
				easeInQuad: function (x, t, b, c, d) {
					return c*(t/=d)*t + b;
				},
				easeOutQuad: function (x, t, b, c, d) {
					return -c *(t/=d)*(t-2) + b;
				},
				easeInOutQuad: function (x, t, b, c, d) {
					if ((t/=d/2) < 1) return c/2*t*t + b;
					return -c/2 * ((--t)*(t-2) - 1) + b;
				},
				easeInCubic: function (x, t, b, c, d) {
					return c*(t/=d)*t*t + b;
				},
				easeOutCubic: function (x, t, b, c, d) {
					return c*((t=t/d-1)*t*t + 1) + b;
				},
				easeInOutCubic: function (x, t, b, c, d) {
					if ((t/=d/2) < 1) return c/2*t*t*t + b;
					return c/2*((t-=2)*t*t + 2) + b;
				},
				easeInQuart: function (x, t, b, c, d) {
					return c*(t/=d)*t*t*t + b;
				},
				easeOutQuart: function (x, t, b, c, d) {
					return -c * ((t=t/d-1)*t*t*t - 1) + b;
				},
				easeInOutQuart: function (x, t, b, c, d) {
					if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
					return -c/2 * ((t-=2)*t*t*t - 2) + b;
				},
				easeInQuint: function (x, t, b, c, d) {
					return c*(t/=d)*t*t*t*t + b;
				},
				easeOutQuint: function (x, t, b, c, d) {
					return c*((t=t/d-1)*t*t*t*t + 1) + b;
				},
				easeInOutQuint: function (x, t, b, c, d) {
					if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
					return c/2*((t-=2)*t*t*t*t + 2) + b;
				},
				easeInSine: function (x, t, b, c, d) {
					return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
				},
				easeOutSine: function (x, t, b, c, d) {
					return c * Math.sin(t/d * (Math.PI/2)) + b;
				},
				easeInOutSine: function (x, t, b, c, d) {
					return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
				},
				easeInExpo: function (x, t, b, c, d) {
					return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
				},
				easeOutExpo: function (x, t, b, c, d) {
					return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
				},
				easeInOutExpo: function (x, t, b, c, d) {
					if (t==0) return b;
					if (t==d) return b+c;
					if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
					return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
				},
				easeInCirc: function (x, t, b, c, d) {
					return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
				},
				easeOutCirc: function (x, t, b, c, d) {
					return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
				},
				easeInOutCirc: function (x, t, b, c, d) {
					if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
					return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
				},
				easeInElastic: function (x, t, b, c, d) {
					var s=1.70158;var p=0;var a=c;
					if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
					if (a < Math.abs(c)) { a=c; var s=p/4; }
					else var s = p/(2*Math.PI) * Math.asin (c/a);
					return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
				},
				easeOutElastic: function (x, t, b, c, d) {
					var s=1.70158;var p=0;var a=c;
					if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
					if (a < Math.abs(c)) { a=c; var s=p/4; }
					else var s = p/(2*Math.PI) * Math.asin (c/a);
					return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
				},
				easeInOutElastic: function (x, t, b, c, d) {
					var s=1.70158;var p=0;var a=c;
					if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
					if (a < Math.abs(c)) { a=c; var s=p/4; }
					else var s = p/(2*Math.PI) * Math.asin (c/a);
					if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
					return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
				},
				easeInBack: function (x, t, b, c, d, s) {
					if (s == undefined) s = 1.70158;
					return c*(t/=d)*t*((s+1)*t - s) + b;
				},
				easeOutBack: function (x, t, b, c, d, s) {
					if (s == undefined) s = 1.70158;
					return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
				},
				easeInOutBack: function (x, t, b, c, d, s) {
					if (s == undefined) s = 1.70158; 
					if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
					return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
				},
				easeInBounce: function (x, t, b, c, d) {
					return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
				},
				easeOutBounce: function (x, t, b, c, d) {
					if ((t/=d) < (1/2.75)) {
						return c*(7.5625*t*t) + b;
					} else if (t < (2/2.75)) {
						return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
					} else if (t < (2.5/2.75)) {
						return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
					} else {
						return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
					}
				},
				easeInOutBounce: function (x, t, b, c, d) {
					if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
					return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
				}
			});
		}
	};

    /*== Define Plugin Jquery ==*/
	$.fn.Mycase = function(options) {
		return this.each(function() {
			new Mycase(this, options).init();
		});
	};

})( jQuery);