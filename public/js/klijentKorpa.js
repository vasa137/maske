const DOSTAVA = 249;
const GRANICA_DOSTAVA = 4;
$korpaInclude = $('#korpaInclude');

function number_format_javascript(n, decimalFields ,decimalSeparator, thousandSeparator) {
    var parts=n.toString().split(".");
    var value =  parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousandSeparator);
    if(decimalFields > 0){
        value += (parts[1] ? decimalSeparator + parts[1].substr(0,decimalFields) : decimalSeparator + '00');
    }
    return value;
}

function postaviUkupnuCenuPorudzbine(total, brojArtikala){
    $('#total-cena').html(number_format_javascript(total, 0, ',', '.') + " rsd");
    let dost = 0;
    $dostavaPolje = $('#dostava');

    if(brojArtikala < GRANICA_DOSTAVA) {
        dost = DOSTAVA;
    }

    $dostavaPolje.html(number_format_javascript(dost, 0, ',', '.') + " rsd")
    $('#total-cena-dostava').html(number_format_javascript(parseFloat(total) + dost, 0, ',', '.') + " rsd");
}

function dodajUKorpu(idProizvod, kolicina, idBrend){
    $.post( "/dodaj_u_korpu?id_proizvod=" + idProizvod + "&kolicina="  + kolicina  + "&id_brend=" + idBrend, function( data ) {
        otvoriDialog();

        if(document.getElementById('korpaInclude') !== undefined) {
            $korpaInclude.html(data.html);
        }
    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        alert('server nije dostupan...');
    });
}

// 1 artikal 900, dva po 800, tri i vise po 700
function azurirajCene(stavke, stavkeTotal){
    var rowIds = Object.keys(stavke);

    for(var i = 0; i < rowIds.length; i++){
        $('#stavka-price-' + rowIds[i]).html(number_format_javascript(stavke[rowIds[i]].price, 0,  ',', '.')+ " rsd");
        $('#stavka-total-' + rowIds[i]).html(number_format_javascript(stavkeTotal[rowIds[i]] , 0,  ',', '.')+ " rsd");
    }
}

// da bi se azurirala korpa u Controlleru potreban je rowId
function azurirajKolicinuUKorpi(rowIdKorpa, idProizvod, kolicina) {
    if (parseInt(kolicina) > 0){
        //$('#qttotal-stavka-' + rowIdKorpa).html(kolicina);
        $.post("/azuriraj_kolicinu_u_korpi?rowId=" + rowIdKorpa +"&id_proizvod=" + idProizvod + "&kolicina=" + kolicina, function( data ) {
            //ISPRAVI SUBTOTAL I TAX NE RADI
            $('#stavka-total-' + rowIdKorpa).html(number_format_javascript(data.stavkaTotal, 0,  ',', '.')+ " rsd");

            azurirajCene(data.stavke, data.stavkeTotal);

            postaviUkupnuCenuPorudzbine(data.total, data.brojArtikala);
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            alert('server nije dostupan...');
        });
    }

}

//zove se iz dijaloga kao linkUspesno
function obrisiIzKorpe(rowIdKorpa, idProizvod){
    $.post("/obrisi_iz_korpe?rowId=" + rowIdKorpa +"&id_proizvod=" + idProizvod, function( data ){
        let brojElemenata = parseInt(data.brojElemenata);

        if(document.getElementById('korpaPrikaz') !== undefined) {
            $('#stavka-proizvod-' + rowIdKorpa).remove();
            if(brojElemenata == 0){
                $('#korpaPrikaz').html('<h3>Korpa je prazna.</h3>');
            } else{
                azurirajCene(data.stavke, data.stavkeTotal);

                postaviUkupnuCenuPorudzbine(data.total, data.brojArtikala);
            }
        }
        if(document.getElementById('korpaInclude') !== undefined){
            $korpaInclude.html(data.html);
        }

        postaviUkupnuCenuPorudzbine(data.total, data.brojArtikala);

        zatvoriDialogSaId(rowIdKorpa);
    }).fail(function (jqXHR, ajaxOptions, thrownError) {
        alert('server nije dostupan...');
    });
}

function ukloniVaucer(rowIdVaucer, idVaucer){
    $.post("/ukloni_vaucer_iz_korpe?rowId=" + rowIdVaucer +"&id_vaucer=" + idVaucer, function( data ) {
        $('#stavka-vaucer-'+rowIdVaucer).remove();

        $('#tabela-vauceri').remove();
        $('#tabele-div').append(data.html);

        postaviUkupnuCenuPorudzbine(data.total, data.brojArtikala);

        zatvoriDialogSaId('vaucer-' + rowIdVaucer);
    }).fail(function (jqXHR, ajaxOptions, thrownError) {
        alert('server nije dostupan...');
    });
}

function primeniKupon(){
    let kod = document.getElementById('kod').value;

    if(kod !== ''){
        $.post("/primeni_kupon_vaucer?kod=" + kod, function( data ) {
            let color = 'color:red;';

            if(data.primenjen) {
                color = 'color:limegreen';

                if(data.kupon_ili_vaucer == 'kupon') {
                    $.each(data.stavke, function (index, element) {
                        if (data.primenjenKupon[element.rowId] !== undefined) {
                            let kupon = data.primenjenKupon[element.rowId];
                            $('#stavka-total-' + element.rowId).html(number_format_javascript(element.subtotal + element.tax*element.qty, 0, ',', '.') + " rsd");
                            $('#kupon-stavka-' + element.rowId).html(kupon.kod + ' (' + kupon.popust * 100 + '%)');
                        }
                    });
                } else if(data.kupon_ili_vaucer == 'vaucer'){
                    $('#tabela-vauceri').remove();
                    $('#tabele-div').append(data.html);
                }

                postaviUkupnuCenuPorudzbine(data.total, data.brojArtikala);
            }

            document.getElementById('kupon-text').style = color;
            $('#kupon-text').html(data.poruka);
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            alert('server nije dostupan...');
        });
    } else{
        $('#kupon-text').html('Kod ne sme biti prazan.');
    }
}
