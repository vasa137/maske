let $selectableTree;
let categoryTree;
let changingNode;

let Category = function(text, tags, nodes) {
    this.text = text;
    this.tags = tags;

    if(nodes != null){
        this.nodes = nodes;
    }
};


var initSelectableTree = function() {
    return $('#treeview-selectable').treeview({
        data: categoryTree,
        multiSelect: false,

    });
};

// kopira dobijeno stablo kategorija u strukturu pogodnu za bootstrap treeview
function buildCategoryTree(categories, kategorija = null, nadkategorija = null){
    let branch = [];

    for(let i = 0; i < categories.length; i++){
        let category = categories[i];
        let children;

        if(category['children'] !== null && category['children'].length > 0){
            children  = buildCategoryTree(category['children'], kategorija, nadkategorija);
        } else{
            children = null;
        }

        let newCat = new Category(category['naziv'], category['id'], children);

        let selected = false;
        let expanded = false;
        let disabled = false;
        if(nadkategorija != null){
            expanded = true;
            if(nadkategorija == category['id']){
                selected = true;
            }
        }

        if(kategorija != null && kategorija == category['id']){
            disabled = true;
            changingNode = newCat;
        }

        newCat.state = {
            'selected' : selected,
            'expanded': expanded,
            'disabled': disabled
        };

        branch.push(newCat);
    }

    return branch;
}

// proizvodKategorije nije null kada se menja proizvod, a jeste kada se dodaje novi
function inicijalizujKategorije(stabloKategorija, kategorija = null,nadkategorija = null){
    stabloKategorija = JSON.parse(stabloKategorija);

    categoryTree = buildCategoryTree(stabloKategorija, kategorija, nadkategorija);

    $selectableTree = initSelectableTree();
}

