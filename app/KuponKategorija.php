<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KuponKategorija extends Model
{
    protected $table = 'kupon_kategorija';

    protected $fillable = ['id_kupon', 'id_kategorija'];

    public static function dohvatiKategorijeZaKupon($id){
        return KuponKategorija::where('id_kupon', $id)->get();
    }

    public function napuni($id_kupon, $id_kategorija){
        $this->id_kupon = $id_kupon;
        $this->id_kategorija = $id_kategorija;

        $this->save();
    }

    public static function obrisiKategorijeZaKupon($id){
        KuponKategorija::where('id_kupon', $id)->delete();
    }
}
