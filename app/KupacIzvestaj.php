<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 04-Apr-19
 * Time: 00:17
 */

namespace App;


class KupacIzvestaj
{
    public $email;
    public $kupac;
    public $grad;
    public $telefon;

    /**
     * KupacIzvestaj constructor.
     * @param $email
     * @param $kupac
     * @param $grad
     * @param $telefon
     */
    public function __construct($email, $kupac, $grad, $telefon)
    {
        $this->email = $email;
        $this->kupac = $kupac;
        $this->grad = $grad;
        $this->telefon = $telefon;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getKupac()
    {
        return $this->kupac;
    }

    /**
     * @param mixed $kupac
     */
    public function setKupac($kupac): void
    {
        $this->kupac = $kupac;
    }

    /**
     * @return mixed
     */
    public function getGrad()
    {
        return $this->grad;
    }

    /**
     * @param mixed $grad
     */
    public function setGrad($grad): void
    {
        $this->grad = $grad;
    }

    /**
     * @return mixed
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * @param mixed $telefon
     */
    public function setTelefon($telefon): void
    {
        $this->telefon = $telefon;
    }


}