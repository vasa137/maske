<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dobavljac extends Model
{
    protected $table = 'dobavljac';

    protected $fillable = ['naziv', 'sifra', 'opis', 'adresa', 'broj_telefona', 'broj_telefona2', 'email', 'email2', 'sakriven', 'PIB', 'fax', 'maticni_broj'];

    protected $appends = ['broj_prodatih', 'broj_na_stanju', 'promet'];

    private $broj_prodatih;
    private $broj_na_stanju;
    private $promet;

    public function setBrojProdatihAttribute($broj_prodatih){
        $this->broj_prodatih = $broj_prodatih;
    }

    public function getBrojProdatihAttribute(){
        return $this->broj_prodatih;
    }

    public function setBrojNaStanjuAttribute($broj_na_stanju){
        $this->broj_na_stanju = $broj_na_stanju;
    }

    public function getBrojNaStanjuAttribute(){
        return $this->broj_na_stanju;
    }

    public function setPrometAttribute($promet){
        $this->promet = $promet;
    }

    public function getPrometAttribute(){
        return $this->promet;
    }

    public static function dohvatiSve(){
        return Dobavljac::where('sakriven', 0)->get();
    }

    public static function dohvatiSaId($id){
        return Dobavljac::where('id', $id)->first();
    }

    public function napuni($naziv, $sifra, $adresa, $grad, $zip, $opis, $broj_telefona, $broj_telefona2, $email, $email2, $fax, $PIB, $maticni_broj){
        $this->naziv = $naziv;
        $this->sifra = $sifra;
        $this->adresa = $adresa;
        $this->grad = $grad;
        $this->zip = $zip;
        $this->opis = $opis;
        $this->broj_telefona = $broj_telefona;
        $this->broj_telefona2 = $broj_telefona2;
        $this->email = $email;
        $this->email2 = $email2;
        $this->fax = $fax;
        $this->PIB = $PIB;
        $this->maticni_broj = $maticni_broj;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }

    public static function dohvatiSveAktivne(){
        return Dobavljac::where('sakriven', 0)->get();
    }

    public static function dohvatiSveObrisane(){
        return Dobavljac::where('sakriven', 1)->get();
    }
}
