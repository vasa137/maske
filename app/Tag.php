<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tag';

    protected $fillable = ['naziv', 'opis', 'sakriven'];

    public static function dohvatiSve(){
        return Tag::all();
    }

    public static function dohvatiSaId($id){
        return Tag::where('id', $id)->first();
    }

    public static function dohvatiSveAktivne(){
        return Tag::where('sakriven', 0)->get();
    }

    public static function dohvatiSveObrisane(){
        return Tag::where('sakriven', 1)->get();
    }

    public function napuni($naziv, $opis){
        $this->naziv = $naziv;
        $this->opis = $opis;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }
}
