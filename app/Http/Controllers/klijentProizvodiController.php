<?php

namespace App\Http\Controllers;

use App\Brend;
use App\Kategorija;
use App\Proizvod;
use App\ProizvodKategorija;
use App\ProizvodSpecifikacija;
use App\Specifikacija;
use App\Utility\Util;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use File;
define("PAGINACIJA", 12);
define("BROJ_POSLEDNJIH", 5);
define("API_DEVICE_DETECT", "http://api.userstack.com/detect");
define("API_DEVICE_DETECT_KEY", "82a560812a90f7e82c3aea23392665db");
class klijentProizvodiController extends Controller
{
    private function detektujUredjaj(Request $request, $brendovi){
        $izabraniBrend = null;

        $client = new Client();

        $deviceDetectResult = json_decode($client->get(API_DEVICE_DETECT,  [
            'query' =>
                [
                    'access_key' => API_DEVICE_DETECT_KEY,
                    'ua' => $request->header('User-Agent')
                ]
        ])->getBody()->getContents());

        //dd($deviceDetectResult);

        if($deviceDetectResult->device->is_mobile_device && $deviceDetectResult->device->type == "smartphone"){
            if($deviceDetectResult->device->brand == "Apple"){
                $deviceFullName = $deviceDetectResult->device->name;
            } else {
                $deviceFullName = $deviceDetectResult->device->brand . " " . $deviceDetectResult->device->name;
            }

            $izabraniBrend = null;

            foreach($brendovi as $brend){
                if(strtolower($brend->naziv) == strtolower($deviceFullName)){
                    $izabraniBrend = $brend->id;
                    break;
                }
            }
        }

        return $izabraniBrend;
    }

    private function dodajPoslednjiUSesiju($proizvod){
        $poslednji = \Session::get('poslednji');
        
        if($poslednji != null){
            if($poslednji[0]->id == $proizvod->id && $poslednji[0]->brend->id == $proizvod->brend->id){
                return;
            }

            if(count($poslednji) < BROJ_POSLEDNJIH)  {
                array_unshift ( $poslednji , $proizvod );
            } else{
                for($i = 4; $i >= 1; $i--){
                    $poslednji[$i] = $poslednji[$i - 1];
                }
                $poslednji[0] = $proizvod;
            }
        }
        else{
            $poslednji = [$proizvod];
        }

        \Session::put('poslednji', $poslednji);
    }


    private function popuniProizvodKategorijomBrendom($proizvod, $izabraniBrend ){
        $proizvodKategorije = ProizvodKategorija::dohvatiKategorijeZaProizvod($proizvod->id);

        $kategorije = [];

        foreach($proizvodKategorije as $proizvodKategorija){
            $kategorije [] = Kategorija::dohvatiSaId($proizvodKategorija->id_kategorija);
        }

        $proizvod->kategorije = $kategorije;

        if($izabraniBrend != null){
            $proizvod->brend = Brend::dohvatiSaId($izabraniBrend);
        }
    }

    private function popuniProizvodeKategorijamaBrendovima($proizvodi, $izabraniBrend ){
        foreach ($proizvodi as $proizvod){
            $this->popuniProizvodKategorijomBrendom($proizvod , $izabraniBrend);
        }
    }


    private function popuniProizvodeNazivimaGlavnihSlika($proizvodi){
        foreach($proizvodi as $proizvod){
            $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);
        }
    }

    public function prodavnica(Request $request){
        $kategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();

        if ($request->ajax()) {
            $queryKategorije = $request->query('kategorije');
            $queryBrend = $request->query('brend');
            $querySort = $request->query('sort');

            if($querySort == "null"){
                $querySort = null;
            }

            $kategorijeZaFilter = [];

            if($queryKategorije != null) {
                foreach ($queryKategorije as $nizKategorija) {
                    $kategorijeZaFilter[] = $nizKategorija;
                }
            }

            $brendoviZaFilter = [];

                if($queryBrend != null){
                $brendoviZaFilter[] = $queryBrend;

                $izabraniBrend = $queryBrend;

                \Session::put('brend',$izabraniBrend);
            }

            $sortBy = "created_at";
            $redosled = "desc";

            if($querySort != null){
                $explodedSort = explode('-', $querySort);

                $sortBy = $explodedSort[0];
                $redosled = $explodedSort[1];
            }

            $proizvodi = Proizvod::filtriraj($kategorijeZaFilter, [], $sortBy, $redosled, PAGINACIJA); // ne filtriraj po brendu

            $this->popuniProizvodeKategorijamaBrendovima($proizvodi, $izabraniBrend);
            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

            $view = view('include.listaProizvoda',compact('proizvodi', 'izabraniBrend'))->render();

            return response()->json(['html'=>$view]);
        } else {
            $brendovi = Brend::dohvatiSveAktivne();

            if(\Session::get('brend') == null){
                $izabraniBrend = $this->detektujUredjaj($request, $brendovi);

                if($izabraniBrend == null){
                    $izabraniBrend = $brendovi[0]->id;
                }

            } else{
                $izabraniBrend = \Session::get('brend');
            }

            \Session::put('brend',$izabraniBrend);

            $proizvodi = Proizvod::filtriraj([], [], 'created_at', 'desc',PAGINACIJA);  // ne filtriraj po brendu

            $stabloKategorija = Util::getInstance()->buildCategoryTree($kategorije, null);

            $this->popuniProizvodeKategorijamaBrendovima($proizvodi , $izabraniBrend);
            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

            return view('prodavnica', compact('stabloKategorija', 'brendovi', 'proizvodi', 'izabraniBrend'));
        }
    }

    public function proizvod($link, $id){
        $proizvod = Proizvod::dohvatiSaId($id);

        if($proizvod == null || $proizvod->sakriven){
            abort(404);
        }

        $brendovi = Brend::dohvatiSveAktivne();

        if(\Session::get('brend') == null){
            $izabraniBrend = $brendovi[0]->id;
        } else{
            $izabraniBrend = \Session::get('brend');
        }

        $this->popuniProizvodKategorijomBrendom($proizvod, $izabraniBrend);

        if($proizvod->ima_specifikacije){
            $proizvodNizSpecifikacija = [];
            $proizvodNizSpecifikacijaTekst = [];
            $proizvodSpecifikacije = ProizvodSpecifikacija::dohvatiSpecifikacijeZaProizvod($proizvod->id);

            foreach ($proizvodSpecifikacije as $proizvodSpecifikacija) {
                $proizvodNizSpecifikacija[] = Specifikacija::dohvatiSaId($proizvodSpecifikacija->id_specifikacija);
                $proizvodNizSpecifikacijaTekst[] = $proizvodSpecifikacija->tekst;
            }

            $proizvod->specifikacije = $proizvodNizSpecifikacija;
            $proizvod->specifikacije_tekst = $proizvodNizSpecifikacijaTekst;
        }

        $proizvodDirectory =  public_path('images/proizvodi/' . $proizvod->id);

        $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);

        $proizvod->sveSlike = Util::getInstance()->pokupiNizFajlova($proizvodDirectory . '/sveSlike');

        $this->dodajPoslednjiUSesiju($proizvod);

        return view('proizvod', compact('proizvod', 'brendovi'));
    }

    public function promeni_brend(Request $request){
        $id_brend = $request->query('id_brend');

        \Session::put('brend',$id_brend);

        $brend = Brend::dohvatiSaId($id_brend);

        return '/images/brendovi/' . $id_brend . '/' . $brend->naziv . '.png';
    }

    public function izaberi_brend(){
        $id_brend = $_GET['id_brend'];

        \Session::put('brend',$id_brend);

        return redirect('/prodavnica');
    }


    public function dizajniraj(){

        $brendovi = Brend::dohvatiSveAktivne();

        $ponudjeneSlikeDirectory = public_path('img/bg');

        $ponudjeneSlike = Util::getInstance()->pokupiNizFajlova($ponudjeneSlikeDirectory );

        $samostalniProizvod = Proizvod::dohvatiSaId(Proizvod::$ID_PRAVLJENA);

        return view('dizajniraj', compact('brendovi', 'ponudjeneSlike', 'samostalniProizvod'));
    }

    public function uploadCaseImage(){
        $image = $_FILES['files'];;

        $directoryPath = public_path('images/maske/temp/' . \Session::getId());

        if(!File::exists($directoryPath)){
            File::makeDirectory($directoryPath, 0755, true);
        }

        //$filename = time() . '.' . pathinfo('' . $image['name'][0], PATHINFO_EXTENSION);

        File::move($image['tmp_name'][0], $directoryPath . '/' . $image['name'][0]);

        chmod($directoryPath . '/' . $image['name'][0], 0644);

        return '/images/maske/temp/' . \Session::getId() .  '/' . $image['name'][0];
    }

    public function uploadFinalCaseImage(){
        // u base64 formatu se putem forme salje slika,pre zareza je opis formata a posle zareza binarni sadrzaj slike
        $image = base64_decode(explode(',', $_POST['img_to_print'])[1]);

        if(\Session::get('id_nove_maske') == null){
            $id_nove_maske = -1;
        }else{
            $id_nove_maske = intval(\Session::get('id_nove_maske')) - 1;
        }

        \Session::put('id_nove_maske', $id_nove_maske);

        $directoryPath = public_path('images/maske/' . \Session::getId() . '/' . $id_nove_maske);


        if(!File::exists($directoryPath)){
            File::makeDirectory($directoryPath, 0755, true);
        }

        file_put_contents($directoryPath . '/maska.png', $image);

        chmod($directoryPath . '/maska.png', 0644);

        return $id_nove_maske;
    }

    public function naslovna(Request $request){
        $brendovi = Brend::dohvatiSveAktivne();

        $izabraniBrend = $this->detektujUredjaj($request, $brendovi);

        return view('welcome', compact('brendovi', 'izabraniBrend'));
    }

    public function izaberi_proizvod_i_brend($id_brend, $id_proizvod){
        \Session::put('brend',$id_brend);

        $proizvod = Proizvod::dohvatiSaId($id_proizvod);

        if($proizvod == null){
            abort(404);
        }

        return redirect('/proizvod/' . str_replace('?', '',str_replace(' ', '-', $proizvod->naziv)) . '/' . $proizvod->id);
    }

    public function insta(Request $request){ //dole je sve kopirano iz funkcije prodavnica
        //treba da ucitava sve proizvode cija je kategorija 1
        $kategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();

        if ($request->ajax()) {
            $queryKategorije = $request->query('kategorije');
            $queryBrend = $request->query('brend');
            $querySort = $request->query('sort');

            if($querySort == "null"){
                $querySort = null;
            }

            $kategorijeZaFilter = [1];

            if($queryKategorije != null) {
                foreach ($queryKategorije as $nizKategorija) {
                    $kategorijeZaFilter[] = $nizKategorija;
                }
            }

            $brendoviZaFilter = [];

                if($queryBrend != null){
                $brendoviZaFilter[] = $queryBrend;

                $izabraniBrend = $queryBrend;

                \Session::put('brend',$izabraniBrend);
            }

            $sortBy = "created_at";
            $redosled = "desc";

            if($querySort != null){
                $explodedSort = explode('-', $querySort);

                $sortBy = $explodedSort[0];
                $redosled = $explodedSort[1];
            }

            $proizvodi = Proizvod::filtriraj($kategorijeZaFilter, [], $sortBy, $redosled, PAGINACIJA); // ne filtriraj po brendu

            $this->popuniProizvodeKategorijamaBrendovima($proizvodi, $izabraniBrend);
            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

            $view = view('include.listaProizvoda',compact('proizvodi', 'izabraniBrend'))->render();

            return response()->json(['html'=>$view]);
        } else {
            $brendovi = Brend::dohvatiSveAktivne();

            if(\Session::get('brend') == null){
                $izabraniBrend = $this->detektujUredjaj($request, $brendovi);

                if($izabraniBrend == null){
                    $izabraniBrend = $brendovi[0]->id;
                }

            } else{
                $izabraniBrend = \Session::get('brend');
            }

            \Session::put('brend',$izabraniBrend);

            $proizvodi = Proizvod::filtriraj([], [], 'created_at', 'desc',PAGINACIJA);  // ne filtriraj po brendu

            $stabloKategorija = Util::getInstance()->buildCategoryTree($kategorije, null);

            $this->popuniProizvodeKategorijamaBrendovima($proizvodi , $izabraniBrend);
            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

            return view('insta', compact('stabloKategorija', 'brendovi', 'proizvodi', 'izabraniBrend'));
        }
    }
}
