<?php

namespace App\Http\Controllers;

use App\Porudzbina;
use App\User;
use App\VrstaKorisnika;
use Illuminate\Http\Request;
use App\StavkaPorudzbina;
use Redirect;
class adminKorisniciController extends Controller
{
    //

	public function korisnik($id)
	{
		$korisnik = User::dohvatiSaId($id);

		if($korisnik == null){
		    abort(404);
        }

		if($korisnik->id_vrsta_korisnika != null){
		    $korisnik->vrsta = VrstaKorisnika::dohvatiSaId($korisnik->id_vrsta_korisnika);
        }

        $this->agregirajPorudzbineZaKorisnike([$korisnik]);

		$vrsteKorisnika = VrstaKorisnika::dohvatiSve();

		return view('admin.adminKorisnik', compact('korisnik', 'vrsteKorisnika'));
	}

	private function agregirajPorudzbineZaKorisnike($korisnici){
        foreach($korisnici as $korisnik){
            $kupacPorudzbine = Porudzbina::dohvatiZaKupca($korisnik->id);

            $brojPorudzbina = 0;
            $iznosPorudzbina = 0;
            $brojProizvoda = 0;

            foreach($kupacPorudzbine as $kupacPorudzbina){
                if($kupacPorudzbina != 'stornirana'){
                    $brojPorudzbina++;
                    $iznosPorudzbina += $kupacPorudzbina->iznos_popust;
                    $brojProizvoda += StavkaPorudzbina::dohvatiBrojProizvodaZaPorudzbinu($kupacPorudzbina->id);
                }
            }

            $korisnik->broj_porudzbina = $brojPorudzbina;
            $korisnik->broj_porucenih_proizvoda = $brojProizvoda;
            $korisnik->promet = $iznosPorudzbina;
            $korisnik->porudzbine = $kupacPorudzbine;
        }
    }

	public function korisnici()
	{
		$korisnici = User::dohvatiNeblokiraneKojiNisuAdmini();

		$korisniciBlokirani = User::dohvatiBlokirane();

		$this->agregirajPorudzbineZaKorisnike($korisnici);
		$this->agregirajPorudzbineZaKorisnike($korisniciBlokirani);

		return view('admin.adminKorisnici', compact('korisnici', 'korisniciBlokirani'));
	}

	public function blokiraj_korisnika($id){
	    $korisnik = User::dohvatiSaId($id);

	    $korisnik->blokiraj();

	    return Redirect::back();
    }

    public function odblokiraj_korisnika($id){
        $korisnik = User::dohvatiSaId($id);

        $korisnik->odblokiraj();

        return Redirect::back();
    }

    public function sacuvaj_korisnika($id){
	    $id_vrsta_korisnika = $_POST['vrstaKorisnika'];
	    $admin_napomena = $_POST['admin_napomena'];

	    if($id_vrsta_korisnika == ''){
	        $id_vrsta_korisnika = null;
        }

	    $korisnik = User::dohvatiSaId($id);

        if($id_vrsta_korisnika != $korisnik->id_vrsta_korisnika || $admin_napomena != $korisnik->admin_napomena){
            $korisnik->azurirajAdminPodatke($id_vrsta_korisnika, $admin_napomena);
        }

        return redirect('/admin/korisnik/' . $id);
    }
}
