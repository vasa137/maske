<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use File;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $direktorijum = public_path('images/maske');
            $sessionDir = storage_path('framework/sessions');
            foreach (scandir($direktorijum) as $file) {
                if ('.' === $file) continue;
                if ('..' === $file) continue;
                if('temp' === $file) continue;

                if(!File::exists($sessionDir . '/' . $file)){
                    File::deleteDirectory($direktorijum . '/' . $file);
                }
            }


            $direktorijumTemp = public_path('images/maske/temp');

            foreach (scandir($direktorijumTemp) as $file) {
                if ('.' === $file) continue;
                if ('..' === $file) continue;

                if(!File::exists($sessionDir . '/' . $file)){
                    File::deleteDirectory($direktorijumTemp . '/' . $file);
                }
            }

        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
